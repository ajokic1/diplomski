import React, { Component } from 'react';
import { Route, Redirect } from 'react-router-dom';

export default function LoginRoute ({ component: Component, isLoggedIn: isLoggedIn, ...rest }) {
    return (
        <Route {...rest} render={(props) => (
            isLoggedIn===false
            ? <Component {...props} {...rest} />
            : <Redirect to='/' />
        )} />
    );
}