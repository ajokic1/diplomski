import React, { Component } from 'react';
import { Route, Redirect } from 'react-router-dom';

export default function PrivateRoute ({ component: Component, isLoggedIn: isLoggedIn, ...rest }) {
    return (
        <Route {...rest} render={(props) => (
            isLoggedIn === true
            ? <Component {...props} {...rest} />
            : <Redirect to='/login' />
        )} />
    );
}
