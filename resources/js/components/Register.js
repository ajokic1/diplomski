import React, { Component } from 'react';
import Banner from './partials/Banner.js';
import axios from 'axios';
import Loading from './partials/Loading.js';

export default class Register extends Component {
    constructor(props) {
        super(props);
        this.state={
            name:'',
            email:'',
            phone:'',
            password:'',
            confirm_password:'',
            pogresanPass: false,
            registering: false,
        }
        this.form = React.createRef();
        this.handleChange=this.handleChange.bind(this);
        this.handleSubmit=this.handleSubmit.bind(this);
    }
    handleChange(event) {
        this.setState({[event.target.name]: event.target.value});
        if(event.target.name==='confirm_password'){
            if(this.state.password !== event.target.value) 
                this.setState({pogresanPass: true});
            else
                this.setState({pogresanPass: false});

        }
    }
    handleSubmit(event) {
        event.preventDefault();
        if(this.form.current.reportValidity()){
            this.register(event);
        }
    }
    register(event) {        
        let formData = new FormData(); 
        formData.append("password", this.state.password);
        formData.append("email", this.state.email);
        formData.append("name", this.state.name);
        formData.append("phone", this.state.phone);
        this.setState({registering: true});

        axios
            .post('api/register', formData)
            .then((json) => {
                if(json.data.success) {
                    let user = {
                        name: json.data.data.name,
                        id: json.data.data.id,
                        email: json.data.data.email,
                        auth_token: json.data.data.auth_token,  
                        phone: json.data.data.phone,
                        timestamp: new Date().toString()
                    };
                    this.props.authSuccess(true, user);
                } else this.props.authSuccess(false);
            }).catch(error => {
                console.log(error);
                this.props.authSuccess(false);
            });
    }
    render() {
        return (
            <Banner>
            <div className='card shadow col-md-12 col-lg-8 mx-auto my-5 px-5'>
                <div className='card-body'>
                    <h2 className='d-block text-center my-4'>Registruj se</h2>
                    <form ref={this.form} className='row needs-validation' method='post' noValidate>
                        <div className='col-sm-6'>
                            <div className='form-group'>
                                <label htmlFor='name'>Ime i prezime:</label>
                                <input id='name' value={this.state.name} onChange={this.handleChange} type='text' className='form-control' name='name' required/>
                                <div className="invalid-feedback">
                                    Unesite ime i prezime.
                                </div>
                            </div>
                            <div className='form-group'>
                                <label htmlFor='email'>E-mail:</label>
                                <input id='email' value={this.state.email} onChange={this.handleChange} type='text' className='form-control' name='email' required/>
                                <div className="invalid-feedback">
                                    Unesite e-mail adresu.
                                </div>
                            </div>
                            <div className='form-group'>
                                <label htmlFor='phone'>Broj telefona:</label>
                                <input id='phone' value={this.state.phone} onChange={this.handleChange} type='text' className='form-control' name='phone' required/>
                                <div className="invalid-feedback">
                                    Unesite broj telefona.
                                </div>
                            </div>
                        </div>
                        <div className='col-sm-6'>
                            <div className='form-group'>
                                <label htmlFor='password'>Password:</label>
                                <input id='password' value={this.state.password} onChange={this.handleChange} type='password' className='form-control' name='password' required/>
                                <div className="invalid-feedback">
                                    Unesite password.
                                </div>
                            </div>
                            <div className='form-group'>
                                <label htmlFor='confirm_password'>Potrvdi password:</label>
                                <input id='confirm_password' value={this.state.confirm_password} onChange={this.handleChange} type='password' className='form-control' name='confirm_password' required/>
                                <div className="invalid-feedback">
                                    Ponovo unesite password.
                                </div>
                                {(this.state.pogresanPass && this.state.confirm_password) &&
                                    <div className="small text-danger">
                                        Ponovo unesite isti password.
                                    </div>
                                }
                            </div>
                            <div className='text-right form-group'>
                            {this.state.registering 
                                ? <Loading/>
                                : <input type='submit' onClick={this.handleSubmit} className='btn btn-primary mt-5' value='Registruj se'/>
                            }
                            
                            
                            </div>
                        </div>
                    </form>
                </div>
            </div>
            </Banner>
        );
    }
}
