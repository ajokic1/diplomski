import React, { Component } from 'react';
import PhotoViewer from './partials/PhotoViewer.js';
import Loading from './partials/Loading.js';

export default class Vehicle extends Component {
    render() {
        const vehicle_id = this.props.match.params.vehicle_id;
        let vehicle = {};
        if(this.props.podaci.vozila){
            vehicle = this.props.podaci.vozila.find(vozilo => vozilo.id == vehicle_id);
        }
        
        return (
            <div className='p-5 container bg-white shadow'>
                {vehicle
                    ? <div className='row'>
                        <div className='col-md-5 rounded p-3'>
                            { this.props.podaci.slike 
                                ? <PhotoViewer photos={this.props.podaci.slike} vehicle_id={vehicle.id}/>
                                : <Loading/>
                            }
                        </div>
                        <div className='col-md-7'>
                            <h1 className='mb-0'>{vehicle.make} {vehicle.modell}</h1>
                            <hr className='mt-0'/>
                            <h2 className='text-primary'>{vehicle.price} €</h2>
                            <div className='row'>
                                <div className='col-sm-6'>
                                    <div><span className='text-muted'>Godište: </span>{vehicle.year}</div>
                                    <div><span className='text-muted'>Kilometraža: </span>{vehicle.km}</div>
                                    <div><span className='text-muted'>Karoserija: </span>{vehicle.body}</div>
                                    <div><span className='text-muted'>Mjenjač: </span>{vehicle.transmission}</div>
                                </div>
                                <div className='col-sm-6'>
                                    <div><span className='text-muted'>Zapremina motora: </span>{vehicle.volume} cm<sup>3</sup></div>
                                    <div><span className='text-muted'>Snaga motora: </span>{vehicle.power} KS ({Math.round(vehicle.power / 1.341)} kW)</div>
                                    <div><span className='text-muted'>Vrsta goriva: </span>{vehicle.fuel}</div>
                                    <div><span className='text-muted'>Potrošnja: </span>{vehicle.consumption} l/100km</div>
                                </div>
                            </div>
                        </div>                
                    </div>
                    : <Loading/>
                }
            </div>
        );
    }
}
