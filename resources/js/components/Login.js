import React, { Component } from 'react';
import Banner from './partials/Banner.js';
import {Link} from 'react-router-dom';
import Loading from './partials/Loading.js';

export default class Login extends Component {
    constructor(props) {
        super(props);
        this.state={
            email:'',
            password:'',
            loggingIn: false,
        }
        this.form = React.createRef();
        this.handleChange = this.handleChange.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);
    }
    handleChange(event) {
        this.setState({
            [event.target.name]: event.target.value,
        });
    }
    handleSubmit(event) {
        event.preventDefault();
        if(this.form.current.reportValidity()){
            this.login(event);
        }
    }
    login(event) {
        let formData = new FormData(); 
        formData.append("password", this.state.password);
        formData.append("email", this.state.email);

        this.setState({loggingIn: true});
        axios
            .post('api/login', formData)
            .then((json) => {
                if(json.data.success) {
                    let user = {
                        name: json.data.data.name,
                        id: json.data.data.id,
                        email: json.data.data.email,
                        auth_token: json.data.data.auth_token,  
                        phone: json.data.data.phone,
                        timestamp: new Date().toString()
                    };
                    this.props.authSuccess(true, user);
                } else this.props.authSuccess(false, {});
            }).catch(error => {
                console.log(error);
                this.props.authSuccess(false, {});
            });
    }
    render() {
        return (
            <Banner>
            <div className='card shadow col-sm-9 col-md-7 col-lg-6 mx-auto my-5 px-5'>
                <div className='card-body'>
                    <h2 className='d-block text-center my-4'>Uloguj se</h2>
                    <form ref={this.form} method='post'>
                        <div className='form-group'>
                            <label>E-mail:</label>
                            <input onChange={this.handleChange} type='text' className='form-control' name='email' required/>
                        </div>
                        <div className='form-group'>
                            <label>Password:</label>
                            <input onChange={this.handleChange} type='password' className='form-control' name='password' required/>
                        </div>
                        <div className='text-center form-group'>
                            <Link to='/register'>Registruj se...</Link>
                            {this.state.loggingIn 
                                ? <Loading/>
                                : <input onClick={this.handleSubmit} type='submit' className='ml-4 btn btn-primary' value='Log in'/>
                            }
                        </div>
                    </form>
                </div>
            </div>
            </Banner>
        );
    }
}
