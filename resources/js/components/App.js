import React, { Component } from 'react';
import ReactDOM from 'react-dom';
import { BrowserRouter as Switch, Route, Link } from "react-router-dom";

import Navbar from './partials/Navbar.js';
import Home from './Home.js';
import Buy from './Buy.js';
import Sell from './Sell.js';
import About from './About.js';
import Login from './Login.js';
import Register from './Register.js';
import Vehicle from './Vehicle.js';
import PrivateRoute from './auth/PrivateRoute.js';
import LoginRoute from './auth/LoginRoute.js';
import ErrorMessage from './partials/ErrorMessage';

const pogoni = [{name:'benzin', id:1}, {name:'dizel', id:2}, {name:'gas', id:3}, 
    {name:'električni', id:4}, {name:'hibrid', id:5}];
const mjenjaci = [{name:'ručni', id:1}, {name:'automatski', id:2}];

const UserContext = React.createContext({});

export default class App extends Component {
    constructor(props){
        super(props);
        const appName="AutoPlac";
        this.state={
            sveUcitano:false,
            isLoggedIn: false,
            user: {},
            errorMessage: '',
            tipVozila: 'car',
            podaci: {
                pogoni: pogoni,
                mjenjaci: mjenjaci,
            },
            constraints: {},
        }
        this.handleTypeClick = this.handleTypeClick.bind(this);        
        this.authSuccess = this.authSuccess.bind(this);
        this.logout = this.logout.bind(this);
        this.addData = this.addData.bind(this);
        this.solveData = this.solveData.bind(this);
        this.setConstraint = this.setConstraint.bind(this);
    }
    setConstraint(name, value) {
        let constraints = this.state.constraints;
        constraints[name]=value;
        this.setState({constraints: constraints});
    }
    addData(name, data) {
        const dataName = {
            modell: 'modeli',
            body: 'karoserije',
            make: 'marke',
            vehicle: 'vozila',
        }
        let podaci = this.state.podaci;
        podaci[dataName[name]].push(data);
        this.setState({podaci:podaci});
        setTimeout(this.solveData, 200);
    }
    handleTypeClick(event) {
        this.setState ({tipVozila: event.target.getAttribute('type')});
    }
    authSuccess(isSuccess, user){
        if(isSuccess){
            this.setState({user: user, isLoggedIn: true});
            localStorage["user"] = JSON.stringify(user);
        } else {
            this.setState({errorMessage: 'Došlo je do greške pri registraciji'});
        }
    }
    componentDidMount() {
        let user = localStorage["user"];
        if (user) {
            user=JSON.parse(user);
            this.setState({ isLoggedIn: true, user: user });
        }
        this.ucitajPodatke();
    }
    ucitajPodatke() {
        let promises = [];
        promises.push(axios
            .get('/api/vehicles')
            .then(json =>{
                let podaci = this.state.podaci;
                podaci['vozila'] = json.data;
                this.setState({podaci: podaci});
            }));
        promises.push(axios
            .get('/api/makes')
            .then(json =>{
                let podaci = this.state.podaci;
                podaci['marke'] = json.data;
                this.setState({podaci: podaci});
            }));
        promises.push(axios
            .get('/api/bodies')
            .then(json =>{
                let podaci = this.state.podaci;
                podaci['karoserije'] = json.data;
                this.setState({podaci: podaci});
            }));
        promises.push(axios
            .get('/api/models')
            .then(json =>{
                let podaci = this.state.podaci;
                podaci['modeli'] = json.data;
                this.setState({podaci: podaci});
            }))
        promises.push(axios
            .get('/api/photos')
            .then(json =>{
                let podaci = this.state.podaci;
                podaci['slike'] = json.data;
                this.setState({podaci: podaci});
            }))

        axios.all(promises).then(axios.spread((...args)=>{
            setTimeout(this.solveData, 50);
        }));
    }
    solveData() {
        const vozila = this.state.podaci.vozila.map(vehicle => {
            vehicle.make = this.state.podaci.marke.find(make => 
                make.id == vehicle.make_id).name;
            vehicle.modell = this.state.podaci.modeli.find(modell => 
                modell.id == vehicle.modell_id).name;
            vehicle.body = this.state.podaci.karoserije.find(body => 
                body.id == vehicle.body_id).name;
            vehicle.photo_path = this.state.podaci.slike.find(slika => 
                slika.vehicle_id == vehicle.id).path;
            return vehicle;
        });
        let podaci = this.state.podaci;
        podaci.vozila = vozila;
        this.setState({podaci: podaci, sveUcitano: true});

    }
    logout(event) {
        event.preventDefault();
        console.log('log out');
        axios
            .post('/api/logout',{}, {headers: {'Authorization': 'Bearer '+this.state.user.auth_token}})
            .then(json =>{
                console.log(json);
            });
        this.setState({
            isLoggedIn: false,
            user: {},
        })
        localStorage['user'] ='';
    }

    render() {
        return (
            <UserContext.Provider value={this.state.user}>
                <Switch>
                    <div className="h-100 pt-5">
                        <Navbar 
                            appName={this.appName} 
                            isLoggedIn={this.state.isLoggedIn} 
                            user={this.state.user} 
                            logout={this.logout}/>
                        {this.state.errorMessage && 
                            <ErrorMessage errorMessage={this.state.errorMessage}/>}
                        <Route 
                            exact path="/" 
                            render={(props) => 
                                <Home
                                    sveUcitano={this.state.sveUcitano}
                                    setConstraint={this.setConstraint} 
                                    type={this.state.tipVozila} 
                                    handleTypeClick={this.handleTypeClick}
                                    podaci={this.state.podaci}/>
                            }
                        />
                        <Route 
                            path="/buy" 
                            render={(props) => 
                                <Buy
                                    constraints={this.state.constraints}
                                    setConstraint={this.setConstraint} 
                                    type={this.state.tipVozila} 
                                    handleTypeClick={this.handleTypeClick}
                                    podaci={this.state.podaci}/>}/>
                        <PrivateRoute 
                            path="/sell" 
                            component={Sell}
                            type={this.state.tipVozila}
                            handleTypeClick={this.handleTypeClick}
                            user={this.state.user}
                            podaci={this.state.podaci}
                            isLoggedIn={this.state.isLoggedIn}
                            addData={this.addData}/>
                        <Route 
                            path="/about" 
                            component={About}/>
                        <LoginRoute 
                            path="/login" 
                            component={Login} 
                            authSuccess={this.authSuccess} 
                            isLoggedIn={this.state.isLoggedIn}/>
                        <LoginRoute 
                            path="/register" 
                            component={Register} 
                            authSuccess={this.authSuccess} 
                            isLoggedIn={this.state.isLoggedIn}/>
                        <Route 
                            path="/vehicle/:vehicle_id" 
                            render={(props) => 
                                <Vehicle {...props} podaci={this.state.podaci}/>
                        }/>
                    </div>
                </Switch>
            </UserContext.Provider>
        );
    }
}

ReactDOM.render(
    <App />,
    document.getElementById('root'));
