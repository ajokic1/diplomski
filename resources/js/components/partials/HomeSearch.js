import React, { Component } from 'react';
import TypeChooser from './TypeChooser.js';
import Input from './Input.js';
import {Link} from 'react-router-dom';
import Loading from './Loading.js';


export default class HomeSearch extends Component {
    constructor(props) {
        super(props);
        this.state={
            make:'',
            body:'',
            transmission:'',
            priceTo:'',
            modell:'',
            fuel:'',
        }
        this.setText = this.setText.bind(this);
    }
    setText(name, value, isOnBlur) {
        this.setState({[name]:value});
        if(isOnBlur && value) {
            this.props.setConstraint(name, value);
        }
        if(isOnBlur && name=='make'){
            if(this.props.podaci.marke){
                const pronadjenaMarka =this.props.podaci.marke.find(marka => 
                    marka.name.toLowerCase() == value.toLowerCase());
                if(pronadjenaMarka) this.setState({make_id:pronadjenaMarka.id});    
            }
            if(!value) this.setState({make_id:''});   
        }
    }
    render() {
        if(!this.props.sveUcitano) return (
            <div className='shadow' style={style}>
                <Loading/>
            </div>
        );
        let listaMarke = [];
        let listaKaroserije = [];
        let listaModeli = [];
        let listaPogoni = [];
        let listaMjenjaci = [];
        if(this.props.podaci.marke){listaMarke = this.props.podaci.marke.filter((marka) => 
            this.props.type == marka.type &&
            (this.state.make 
                ? marka.name.toLowerCase().includes(this.state.make.toLowerCase())
                : true
            ));}
        if(this.props.podaci.karoserije){listaKaroserije = this.props.podaci.karoserije.filter((karoserija) => 
            this.props.type == karoserija.type && 
            (this.state.body 
                ? karoserija.name.toLowerCase().includes(this.state.body.toLowerCase())
                : true
            ));}
        if(this.props.podaci.modeli){listaModeli = this.props.podaci.modeli.filter((model) => 
            this.state.make_id == model.make_id &&
            (this.state.modell
                ? model.name.toLowerCase().includes(this.state.modell.toLowerCase())
                : true
            ));}
        if(this.props.podaci.pogoni){listaPogoni = this.props.podaci.pogoni.filter((pogon) =>
            (this.state.fuel 
                ? pogon.name.toLowerCase().includes(this.state.fuel.toLowerCase())
                : true
            ));}
        if(this.props.podaci.mjenjaci){listaMjenjaci = this.props.podaci.mjenjaci.filter((mjenjac) =>
            (this.state.transmission 
                ? mjenjac.name.toLowerCase().includes(this.state.transmission.toLowerCase())
                : true
            ));}
        return (
            <div className='shadow' style={style}>
                <div className='row'>
                    <div className='col-md-12 text-muted'>
                        <TypeChooser spacing='mx-2' handleTypeClick={this.props.handleTypeClick} type={this.props.type}/>
                    </div>                
                    <div className='col-md-6 px-2'>
                        <Input setText={this.setText} inputText={this.state.make} caption='Marka:' name='make'  lista={listaMarke}/>                       
                        <Input setText={this.setText} inputText={this.state.body} caption='Karoserija:' name='body' lista={listaKaroserije}/>
                        <Input unit='€' setText={this.setText} inputText={this.state.priceTo} caption='Cijena do:' name='priceTo'/>
                    </div>
                    <div className='col-md-6 px-2'>
                        <Input setText={this.setText} inputText={this.state.modell} caption='Model:' name='modell' lista={listaModeli}/>
                        <Input setText={this.setText} inputText={this.state.fuel} caption='Pogon:' name='fuel' lista={listaPogoni}/>                        
                        <Input setText={this.setText} inputText={this.state.transmission} caption='Mjenjač:' name='transmission' lista={listaMjenjaci}/>                  
                    </div>
                </div>
                <div className='row mt-4'>
                    <div className='col-md-6 px-2 pt-2'>
                        <Link to='/buy'>Detaljna pretraga</Link>
                    </div>
                    <div className='col-md-6 px-2'>
                        <Link to='/buy'><button className='btn btn-primary w-100'>Pretraga</button></Link>    
                    </div>
                </div>
            </div>
        );
    }
}
const style={
    margin: '5rem 2rem 2rem 2rem',
    padding: '1.8rem',
    backgroundColor: 'white',
    maxWidth: '25rem',
    height: 'auto',
    borderRadius: '5pt',
}
