import React, { Component } from 'react';
import TypeIcon from './TypeIcon';

export default function TypeChooser (props) {
        return (
            <div className={'text-center ' + (props.size ? props.size : 'h2')}>
                <TypeIcon type='car' selected={props.type==='car'?true:false} handleTypeClick={props.handleTypeClick} spacing={props.spacing}/>
                <TypeIcon type='motorcycle' selected={props.type==='motorcycle'?true:false} handleTypeClick={props.handleTypeClick} spacing={props.spacing}/>
                <TypeIcon type='truck' selected={props.type==='truck'?true:false} handleTypeClick={props.handleTypeClick} spacing={props.spacing}/>
            </div>
        );
}
