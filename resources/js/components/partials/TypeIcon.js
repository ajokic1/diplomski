import React, { Component } from 'react';

export default function TypeIcon(props) {
    const cls = props.spacing + ' type_icon fas fa-'+props.type + (props.selected ? ' text-primary' : '');
    return (
        <span type={props.type} onClick={props.handleTypeClick} className={cls}></span>
    );
}
