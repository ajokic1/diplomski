import React, { Component } from 'react';

export default class Banner extends Component {
    render() {
        return (
            <div style={style}>
                <div className='container'  style={{ overflow: 'auto', }}>
                    {this.props.children}
                    }
                </div>
            </div>
        );
    }
}
const style = {
    width: '100vw',
    height: '100%',
    backgroundImage: 'url("../images/banner.jpg")',
    backgroundPosition: 'center',
    backgroundSize: 'cover',
}