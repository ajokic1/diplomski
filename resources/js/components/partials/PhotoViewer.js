import React, { Component } from 'react';
import Photo from './Photo.js';

export default class PhotoViewer extends Component {
    constructor(props) {
        super(props);
        this.state={
            selPhoto: '',
        }
        this.handlePhotoClick = this.handlePhotoClick.bind(this);
    }
    handlePhotoClick(path, id){
        const selPhoto = {path, id};
        this.setState({ selPhoto });
    }
    render() {
        let photosList = [];
        if(this.props.photos && this.props.vehicle_id){
            photosList = this.props.photos
                .filter(photo => photo.vehicle_id == this.props.vehicle_id)
                .map(photo =>{
                    if(!this.state.selPhoto) this.setState({selPhoto: photo});
                    return <Photo key={photo.id} photo={photo} handlePhotoClick={this.handlePhotoClick}/>
                });
        }
        return (
            <div>
                {this.state.selPhoto && <img src={'/storage/' + this.state.selPhoto.path} className='w-100'/>}
                <div className='d-flex flex-wrap'>
                    {photosList}
                </div>
            </div>
        );
    }
}

