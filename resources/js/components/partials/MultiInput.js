import React, { Component } from 'react';
import DropDown from './DropDown.js';

export default class MultiInput extends Component {
    constructor(props) {
        super(props);
        this.state = {
            focused: false,
            inputText: '',
            izabraniElement: 0,
        }
        this.itemClicked = this.itemClicked.bind(this);
        this.onChange = this.onChange.bind(this);
        this.onKeyPress = this.onKeyPress.bind(this);
        this.onBlur = this.onBlur.bind(this);
        this.onFocus = this.onFocus.bind(this);
    }
    onChange(event) {
        const fLista = this.props.lista.filter((element) =>
            element.name.toLowerCase().includes(event.target.value.toLowerCase()));
        this.setState({
            filtriranaLista: fLista,
        });
        this.props.setText(this.props.name, event.target.value);
    }
    onKeyPress(event) {
        if(this.props.lista.length>0){
            if(event.key==='Enter'){
                this.setState({
                    focused: false,
                });
                this.props.setText(this.props.name, this.props.lista[0].name, true);
            }
        }
    }
    itemClicked(event) {
        const id = event.currentTarget.getAttribute('id');
        const text = event.currentTarget.innerText;
        this.setState({
            focused: false});
        this.props.setText(this.props.name, text, true);
    }
    onBlur(event) {
        this.props.setText(this.props.name, event.target.value, true)
        setTimeout(()=>this.setState({focused: false}), 500);
    }
    onFocus(event) {
        this.setState({focused:true});
    }
    render() {
        return (
            <div className='position-relative'>
                <input disabled={this.props.disabled} onFocus={this.onFocus} onBlur={this.onBlur} onKeyPress={this.onKeyPress} onChange={this.onChange} value={this.props.inputText}  style={this.props.inputStyle} className='w-100 p-1 position-relative' type='text' name={this.props.name}/>
                <span className='fas fa-angle-down position-relative float-right text-muted' style={{top: '-1.35rem', right: '0.5rem'}}></span>
                {
                    this.state.focused &&
                    <DropDown name={this.props.name} addData={this.props.addData} inputText={this.props.inputText} lista={this.props.lista} itemClicked={this.itemClicked}/>
                }
            </div>
        );
    }
}
