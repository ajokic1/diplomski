import React, { Component } from 'react';
import MultiInput from './MultiInput.js';
import TextInput from './TextInput.js';

export default class Input extends Component {
    constructor(props) {
        super(props);
    }
    render() {
        const inputStyle = this.props.white ? inputStyleWhite : inputStyleNoBg;
        return (
            <div className='mt-2'>
                <small>{this.props.caption}&nbsp;</small>
                {this.props.lista
                    ?<MultiInput
                        disabled={this.props.disabled}
                        name={this.props.name}  
                        lista={this.props.lista} 
                        inputStyle={inputStyle}
                        inputText={this.props.inputText}
                        setText={this.props.setText}
                        addData={this.props.addData}/>
                    :<TextInput 
                        inputStyle={inputStyle}
                        name={this.props.name}
                        inputText={this.props.inputText}
                        setText={this.props.setText}
                        unit={this.props.unit}/>
                }
            </div>
        );
    }
}
const inputStyleWhite = {
    borderStyle: 'solid',
    borderWidth: '1px',
    borderRadius: '4px',
    borderColor: 'lightGray',
    backgroundColor: 'white',
}
const inputStyleNoBg = {
    borderStyle: 'solid',
    borderWidth: '1px',
    borderRadius: '4px',
    borderColor: 'lightGray',
    backgroundColor:'transparent',
}