import React, { Component } from 'react';

export default function ErrorMessage(props){
    return (
        <div class='card position-fixed' style={{top:'3rem', right:'1rem', height:'3rem'}}>
            <div class='card-body'>
                {this.props.errorMessage}
            </div>
        </div>
    );
}
