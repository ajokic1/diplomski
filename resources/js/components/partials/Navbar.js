import React, { Component } from 'react';
import { Link, withRouter } from 'react-router-dom';
import NavUser from './NavUser.js';


class Navbar extends Component {
    render() {
        return (
            <nav className="navbar navbar-expand-md fixed-top navbar-dark bg-dark">
                    <Link className="navbar-brand" to="/">AutoPlac.me</Link>
                    <button className="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNav" aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
                        <span className="navbar-toggler-icon"></span>
                    </button>
                    <div className="collapse navbar-collapse" id="navbarNav">
                        <ul className="navbar-nav mr-auto">
                            <li 
                                className={'nav-item' + 
                                    (this.props.location.pathname==='/' && ' active')}>
                                <Link className="nav-link" to="/">
                                    Početna
                                </Link>
                            </li>
                            <li 
                                className={'nav-item' + 
                                    (this.props.location.pathname==='/buy' && ' active')}>
                                <Link className="nav-link" to="/buy">
                                    Kupi
                                </Link>
                            </li>
                            <li 
                                className={'nav-item' + 
                                    (this.props.location.pathname==='/sell' && ' active')}>
                                <Link className="nav-link" to="/sell">
                                    Prodaj
                                </Link>
                            </li>
                        </ul>
                        <div>
                            <NavUser isLoggedIn={this.props.isLoggedIn} user={this.props.user} logout={this.props.logout}/>
                        </div>
                    </div>                    
            </nav>
        );
    }
}

export default withRouter(Navbar);