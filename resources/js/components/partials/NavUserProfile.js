import React, { Component } from 'react';
import { Link } from 'react-router-dom';

export default function NavUserProfile(props) {
    return (
        <ul className='navbar-nav'>
            
            <li className="nav-item mx-1"><Link to='/profile' className="nav-link">
                {props.user.name}
            </Link></li>
            <li className="nav-item mx-1"><a href='#' onClick={props.logout} className="nav-link">
                <i className='fas fa-user-times'/>&nbsp;
                Log out
            </a></li>
        </ul>
    );
}
