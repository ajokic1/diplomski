import React, { Component } from 'react';
import { Link } from 'react-router-dom';

export default function NavUserLogin(props) {
    return (
        <ul className='navbar-nav'>
            <li className="nav-item mx-1"><Link className="nav-link" to="/login">
                <i className='fas fa-sign-in-alt'/>&nbsp;
                Log in
            </Link></li>
            <li className="nav-item mx-1"><Link className="nav-link" to="/register">
                <i className='fas fa-user-plus'/>&nbsp;
                Registruj se
            </Link></li>
        </ul>
    );

}
