import React, { Component } from 'react';
import { Link } from 'react-router-dom';

export default class VehicleCard extends Component {
    
    render() {
        const photoStyle = {
            backgroundImage: 'url("storage/'+this.props.vehicle.photo_path+'")',
            height: '10rem',
            width: '100%',
            backgroundPosition: 'center',
            backgroundSize: 'cover',
        }
        return (
            <div className='col-lg-3 col-md-4 col-sm-12 m-0 p-2'>
            <Link to={'/vehicle/'+this.props.vehicle.id} style={{ textDecoration: 'none', color: 'black' }}>
            <div className="card bg-light dark_hover" style={{width: '100%'}}>
                <div 
                    style={photoStyle}
                    ></div>
                <div className="card-body">
                    <h4 className="card-title">{this.props.vehicle.make} {this.props.vehicle.modell}</h4>
                    <div className='small'>{this.props.vehicle.year} god., {this.props.vehicle.km} km</div>
                    <div className='small'>Motor: {this.props.vehicle.volume} cm<sup>3</sup>, {this.props.vehicle.fuel} </div>
                    <div className='small'>Snaga: {this.props.vehicle.power} KS</div>                    
                    <div className='small'>Potrošnja: {this.props.vehicle.consumption} {this.props.vehicle.consumption && 'l/100km'}</div>
                    <div className='small'>Karoserija: {this.props.vehicle.body}</div>
                    <div className='small'>Mjenjač: {this.props.vehicle.transmission}</div>
                    <div className='text-right text-primary h5 strong'>{this.props.vehicle.price} €</div>
                </div>
            </div>
            </Link>
            </div>
        );
    }
}
