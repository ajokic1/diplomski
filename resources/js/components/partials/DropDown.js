import React, { Component } from 'react';

export default class DropDown extends Component {
    render() {
        let lista = this.props.lista.map((el) =>
            <li 
                onClick={this.props.itemClicked} 
                className='small multi-li'
                id={el.id}
                key={el.id}>{el.name}
            </li>);
        if(this.props.addData && this.props.lista.length===0) lista.push(
            <li
                onClick={this.props.addData}
                className='small multi-li text-primary'
                key={-1}
                name={this.props.name}
                >Dodaj "{this.props.inputText}"
            </li>
        );
        return (
            <div 
                className='w-100 overflow-auto position-absolute bg-white border rounded shadow' 
                style={{maxHeight: '8rem', zIndex: '1000', top: '2.5rem'}}>
                <ul className='multi-ul'>
                    {lista}
                </ul>
            </div>
        );
    }
}

