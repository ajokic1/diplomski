import React, { Component } from 'react';
import Input from './Input.js';
import TypeChooser from './TypeChooser.js';

export default class Sidebar extends Component {
    constructor(props) {
        super(props);
        this.state = {
            make:'',
            body:'',
            fuel:'',
            transmission:'',
            priceFrom:'',
            priceTo:'',
            yearFrom:'',
            yearTo:'',
            kmFrom:'',
            kmTo:'',
            modell:'',
            consumptionTo:'',
            volumeFrom:'',
            volumeTo:'',
            powerFrom:'',
            powerTo:'',
        }
        this.setText=this.setText.bind(this);
    }
    componentDidMount() {
        for(let constraint in this.props.constraints) {
            this.setState({[constraint]: this.props.constraints[constraint]});
        }
    }
    setText(name, value, isOnBlur) {
        this.setState({[name]:value});
        if(isOnBlur) {
            this.props.setConstraint(name, value);
        }
        if(isOnBlur && name=='make'){
            if(this.props.podaci.marke){
                const pronadjenaMarka =this.props.podaci.marke.find(marka => 
                    marka.name.toLowerCase() == value.toLowerCase());
                if(pronadjenaMarka) this.setState({'make_id':pronadjenaMarka.id});    
            }
            
        }
    }
    render() {
        let listaMarke = [];
        let listaKaroserije = [];
        let listaModeli = [];
        let listaPogoni = [];
        let listaMjenjaci = [];
        if(this.props.podaci.marke){listaMarke = this.props.podaci.marke.filter((marka) => 
            this.props.type == marka.type &&
            (this.state.make 
                ? marka.name.toLowerCase().includes(this.state.make.toLowerCase())
                : true
            ));}
        if(this.props.podaci.karoserije){listaKaroserije = this.props.podaci.karoserije.filter((karoserija) => 
            this.props.type == karoserija.type && 
            (this.state.body 
                ? karoserija.name.toLowerCase().includes(this.state.body.toLowerCase())
                : true
            ));}
        if(this.props.podaci.modeli){listaModeli = this.props.podaci.modeli.filter((model) => 
            this.state.make_id == model.make_id &&
            (this.state.modell
                ? model.name.toLowerCase().includes(this.state.modell.toLowerCase())
                : true
            ));}
        if(this.props.podaci.pogoni){listaPogoni = this.props.podaci.pogoni.filter((pogon) =>
            (this.state.fuel 
                ? pogon.name.toLowerCase().includes(this.state.fuel.toLowerCase())
                : true
            ));}
        if(this.props.podaci.mjenjaci){listaMjenjaci = this.props.podaci.mjenjaci.filter((mjenjac) =>
            (this.state.transmission 
                ? mjenjac.name.toLowerCase().includes(this.state.transmission.toLowerCase())
                : true
            ));}
        return (            
            <div className='px-4 pt-3'>
                <div className='mt-3 text-muted'>
                    <TypeChooser spacing='mx-2' type={this.props.type} handleTypeClick={this.props.handleTypeClick}/>
                </div>
                <div className='row'>
                    <div className='col-md-6 px-1'>
                        <Input 
                            caption='Marka:' 
                            name='make' 
                            setText={this.setText}
                            inputText={this.state.make}
                            lista={listaMarke}
                            white={this.props.constraints.make ? true : false}/>
                        <Input 
                            caption='Karoserija:' 
                            name='body' 
                            setText={this.setText}
                            inputText={this.state.body}
                            lista={listaKaroserije}
                            white={this.props.constraints.body ? true : false}/>                       
                        <Input 
                            caption='Mjenjač:' 
                            name='transmission' 
                            setText={this.setText}
                            inputText={this.state.transmission}
                            lista={listaMjenjaci}
                            white={this.props.constraints.transmission ? true : false}/>
                    </div>
                    <div className='col-md-6 px-1'>
                        <Input 
                            caption='Model:' 
                            name='modell' 
                            setText={this.setText}
                            inputText={this.state.modell}
                            lista={listaModeli}
                            white={this.props.constraints.modell ? true : false}/>
                        <Input 
                            caption='Pogon:' 
                            name='fuel' 
                            setText={this.setText}
                            inputText={this.state.fuel}
                            lista={listaPogoni}
                            white={this.props.constraints.fuel ? true : false}/> 
                        <Input 
                            caption='Potrošnja do:' 
                            name='consumptionTo' 
                            setText={this.setText}
                            inputText={this.state.consumptionTo}
                            white={this.props.constraints.consumptionTo ? true : false}/>                         
                        
                    </div>
                </div>
                <div className='row'>
                    <div className='col-md-6 px-1'>
                        <Input
                            unit='€' 
                            caption='Cijena od:' 
                            name='priceFrom' 
                            setText={this.setText}
                            inputText={this.state.priceFrom}
                            white={this.props.constraints.priceFrom ? true : false}/>
                        <Input 
                            caption='Godište od:' 
                            name='yearFrom' 
                            setText={this.setText}
                            inputText={this.state.yearFrom}
                            white={this.props.constraints.yearFrom ? true : false}/>
                        <Input 
                            unit='km'
                            caption='Kilometraža od:' 
                            name='kmFrom' 
                            setText={this.setText}
                            inputText={this.state.kmFrom}
                            white={this.props.constraints.kmFrom ? true : false}/>
                        <Input 
                            unit='cm3'
                            caption='Zapremina od:' 
                            name='volumeFrom' 
                            setText={this.setText}
                            inputText={this.state.volumeFrom}
                            white={this.props.constraints.volumeFrom ? true : false}/>
                        <Input
                            unit='KS' 
                            caption='Snaga od:' 
                            name='powerFrom' 
                            setText={this.setText}
                            inputText={this.state.powerFrom}
                            white={this.props.constraints.powerFrom ? true : false}/>
                    </div>
                    <div className='col-md-6 px-1'>
                        <Input
                            unit='€' 
                            caption='do:' 
                            name='priceTo' 
                            setText={this.setText}
                            inputText={this.state.priceTo}
                            white={this.props.constraints.priceTo ? true : false}/>
                        <Input 
                            caption='do:' 
                            name='yearTo' 
                            setText={this.setText}
                            inputText={this.state.yearTo}
                            white={this.props.constraints.yearTo ? true : false}/>
                        <Input 
                            unit='km'
                            caption='do:' 
                            name='kmTo' 
                            setText={this.setText}
                            inputText={this.state.kmTo}
                            white={this.props.constraints.kmTo ? true : false}/>
                        <Input 
                            unit='cm3'
                            caption='do:' 
                            name='volumeTo' 
                            setText={this.setText}
                            inputText={this.state.volumeTo}
                            white={this.props.constraints.volumeTo ? true : false}/>
                        <Input
                            unit='KS' 
                            caption='do:' 
                            name='powerTo' 
                            setText={this.setText}
                            inputText={this.state.powerTo}
                            white={this.props.constraints.powerTo ? true : false}/>
                    </div>
                </div>
            </div>
        );
    }
}
