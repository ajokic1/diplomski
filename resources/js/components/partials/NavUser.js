import React, { Component } from 'react';
import NavUserLogin from './NavUserLogin.js';
import NavUserProfile from './NavUserProfile.js';


export default function NavUser(props) {
    return (
        <div>
        {
            props.isLoggedIn ?
            <NavUserProfile user={props.user} logout={props.logout}/> : <NavUserLogin />
        }
        </div>
    );
}
