import React, { Component } from 'react';

export default class Photo extends Component {
    constructor(props) {
        super(props);
        this.handlePhotoClick = this.handlePhotoClick.bind(this);
    }
    handlePhotoClick() {
        this.props.handlePhotoClick(this.props.photo.path, this.props.photo.id);
    }
    render() {
        const photoStyle = {
            backgroundImage: 'url(/storage/'+(this.props.photo ? this.props.photo.path : '')+')',
            backgroundPosition: 'center',
            backgroundSize: 'cover',
            width: '100%',
            maxWidth: '10rem',
            height: '7rem'
        }
        return(
            <div className='col-lg-4 col-md-6 col-sm-12 p-1'>
                <div onClick={this.handlePhotoClick} className='mx-auto text-white h1 m-1 rounded' style={photoStyle}>
                    {this.props.remove && <div onClick={this.handlePhotoClick} className='h-100 w-100 rounded text-center delete-overlay' style={{backgroundColor:'black'}}>
                        <span onClick={this.handlePhotoClick} className='fas fa-trash'></span>
                    </div>}
                </div>
            </div>
        );
    }
}