import React, { Component } from 'react';

export default class TextInput extends Component {
    constructor(props) {
        super(props);
        this.handleChange = this.handleChange.bind(this);
        this.onBlur = this.onBlur.bind(this);
        this.onKeyPress = this.onKeyPress.bind(this);
    }
    handleChange(event) {
        this.props.setText(this.props.name, event.target.value);
    }
    onBlur(event) {
        this.props.setText(this.props.name, event.target.value, true)
    }
    onKeyPress(event) {
        if(event.key==='Enter'){
            this.props.setText(this.props.name, event.target.value, true);
        }
    }
    render() {
        return (
            <div>
                <input onBlur={this.onBlur} onKeyPress={this.onKeyPress} style={this.props.inputStyle} value={this.props.inputText} onChange={this.handleChange} className='w-100 p-1' type='text' name={this.props.name}/>
                <span 
                    className='position-relative float-right text-muted' 
                    style={{top: '-1.70rem', right: '0.7rem', textAlign:'right', fontStyle:'italic'}}>
                        <small>{this.props.unit}</small>
                </span>
            </div>
        );
    }
}
