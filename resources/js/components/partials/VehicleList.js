import React, { Component } from 'react';
import VehicleCard from './VehicleCard.js';
import Loading from './Loading.js';

export default class VehicleList extends Component {
    constructor(props) {
        super(props);
    }
    filtriraj(vehicle){
        const constraints = this.props.constraints;
        let result=true;
        if(vehicle.type != this.props.type) return false;
        for (let constraint in constraints) {
            if(constraints[constraint]){    
                if(constraint.endsWith('From')){
                    const name = constraint.substr(0, constraint.length-4);
                    if(vehicle[name] < parseInt(constraints[constraint])) result=false;
                }else if(constraint.endsWith('To')){
                    const name = constraint.substr(0, constraint.length-2);
                    if(vehicle[name] > parseInt(constraints[constraint])) result=false;
                } else {
                    if(!vehicle[constraint].toLowerCase()
                        .includes(constraints[constraint].toLowerCase())) result=false;
                }
            }
        }
        return result;
    }
    render() {
        let filtriranaLista=[];
        if(this.props.list) {
            filtriranaLista = this.props.list.filter(vehicle => this.filtriraj(vehicle));
        }
        let vehicleList=[];
        if(filtriranaLista) {vehicleList = filtriranaLista.map(vehicle => 
            <VehicleCard key={vehicle.id} vehicle={vehicle} />);}  
        return (
            <div className='p-2 pt-3 row w-100'>
                {vehicleList ? vehicleList : <Loading/>}
            </div>
        );
    }
}
