import React, { Component } from 'react';
import Photo from './Photo.js';

export default class AddPhotos extends Component {
    constructor(props) {
        super(props);
        this.fileInput = React.createRef();
        this.handleChange = this.handleChange.bind(this);
    }
    handleChange(event) {
        this.props.uploadPhotos(this.fileInput.current.files);
    }
    render() {
        const photos = this.props.photos.map(photo =>
            <Photo handlePhotoClick={this.props.handlePhotoClick} photo={photo} remove/>);
        return (
            <div className='bg-light border p-4 mt-4 rounded'>
                <input multiple onChange={this.handleChange} style={{visibility: 'hidden', width: 0, height: 0}} id='file' type='file' ref={this.fileInput} />
                <label htmlFor='file' className='btn btn-primary'>
                    <span className='fas fa-plus'></span>&nbsp;&nbsp;Dodaj fotografije
                </label>
                <div className='row'>
                    {photos}
                </div>
            </div>
        );
    }
}
