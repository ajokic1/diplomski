import React, { Component } from 'react';
import VehicleCard from './partials/VehicleCard.js';
import Sidebar from './partials/Sidebar.js';
import VehicleList from './partials/VehicleList.js';

export default class Buy extends Component {
    render() {
        return (
            <div className='row h-100 w-100 mx-0'>
                <div className='overflow-auto col-lg-3 col-md-4 col-sm-5  bg-light border border-bottom-0 border-top-0 border-left-0 h-100'>
                    <Sidebar
                        constraints={this.props.constraints}
                        podaci={this.props.podaci}
                        setConstraint={this.props.setConstraint} 
                        type={this.props.type} 
                        handleTypeClick={this.props.handleTypeClick} />
                </div>                
                <div className='overflow-auto col-lg-9 col-md-8 col-sm-7 bg-white h-100'>
                    <VehicleList
                        type={this.props.type} 
                        constraints={this.props.constraints}
                        list={this.props.podaci.vozila}/>
                </div>
            </div>            
        );
    }
}

