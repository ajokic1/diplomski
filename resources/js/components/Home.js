import React, { Component } from 'react';
import Banner from './partials/Banner.js';
import HomeSearch from './partials/HomeSearch.js';

export default class Home extends Component {
    render() {
        return (
            <div className='h-100'>
                <Banner>
                    <HomeSearch
                        sveUcitano={this.props.sveUcitano} 
                        setConstraint={this.props.setConstraint}
                        type={this.props.type} 
                        handleTypeClick={this.props.handleTypeClick}
                        podaci={this.props.podaci}/>
                </Banner>
            </div>
        );
    }
}
