import React, { Component } from 'react';
import TypeChooser from './partials/TypeChooser.js';
import Input from './partials/Input.js';
import AddPhotos from './partials/AddPhotos.js';
import {Redirect} from 'react-router-dom';
import axios from 'axios';

const vrsteVozilaSr = {
    car: 'automobil',
    motorcycle: 'motor',
    truck: 'kamion'
}
const dataName = {
    modell: 'modeli',
    body: 'karoserije',
    make: 'marke',
}
const obaveznaPolja = 
    ['make_id', 'body_id', 'modell_id', 'price', 'year', 'transmission', 'fuel'];

export default class Sell extends Component {
    constructor(props) {
        super(props);
        this.state = {
            make:'',
            body:'',
            transmission:'',
            price:'',
            year:'',
            km:'',
            modell:'',
            consumption:'',
            volume:'',
            power:'',
            fuel:'',
            toHome: false,
            photos:[],
            uploaded: false,
            uploading: false,
        }
        this.setText = this.setText.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);
        this.uploadPhotos = this.uploadPhotos.bind(this);
        this.uploadPhoto = this.uploadPhoto.bind(this);
        this.deletePhoto = this.deletePhoto.bind(this);
        this.handlePhotoClick = this.handlePhotoClick.bind(this);
        this.addData = this.addData.bind(this);
    }
    componentWillUnmount() {
        if(!this.state.uploaded) this.state.photos.map(this.deletePhoto);
    }
    deletePhoto(photo) {
        var formData = new FormData();
            formData.append('path', photo.path);
            axios.post('api/photos/temp/delete', formData, {
                headers: {
                    'Authorization': 'Bearer '+this.props.user.auth_token,
                }
            }).then((json) => {
                console.log(json.data);
            }) 
    }
    handlePhotoClick(path) {
        this.deletePhoto({path: path});
        const newPhotos = this.state.photos.filter(photo => photo.path!==path);
        this.setState({photos: newPhotos});
    }
    setText(name, value, isOnBlur) {
        this.setState({[name]: value});
        //if(id !== undefined) this.setState({[name + '_id']: id});
        if(isOnBlur && value && dataName[name]) {
            const podatak = this.props.podaci[dataName[name]].find(el => 
                el.name.toLowerCase()===value.toLowerCase());
            if(podatak)
                this.setState({[name+'_id']:podatak.id, [name]: podatak.name});
            else
                this.setState({[name+'_id']:''});
        }
    }
    uploadPhotos(photos) {
        Array.from(photos).map(this.uploadPhoto);
    }
    uploadPhoto(photo) {
        var formData = new FormData();
        formData.append('photo',  photo);
        axios
            .post('api/photos/temp', formData, {
                headers: {
                    'Authorization': 'Bearer '+this.props.user.auth_token,
                    'Content-Type': 'multipart/form-data'
                }
            }).then(json => {
                var photos = this.state.photos.slice();
                photos.push({path: json.data});
                this.setState({photos: photos});
            });
    }
    handleSubmit () {
        event.preventDefault();
        
        let formData = new FormData(); 
        formData.append('make_id', this.state.make_id);
        formData.append('user_id', this.props.user.id);
        formData.append('body_id', this.state.body_id);
        formData.append('transmission', this.state.transmission);
        formData.append('price', this.state.price);
        formData.append('year', this.state.year);
        formData.append('km', this.state.km);
        formData.append('modell_id', this.state.modell_id);
        formData.append('consumption', this.state.consumption);
        formData.append('volume', this.state.volume);
        formData.append('power', this.state.power);
        formData.append('fuel', this.state.fuel);
        formData.append('type', this.props.type);

        axios
            .post(
                'api/vehicles',
                formData, 
                {headers: {'Authorization': 'Bearer '+this.props.user.auth_token}})
            .then(json => {
                    this.props.addData('vehicle', json.data);
                    this.submitPhotos(json.data.id);
                    
            });
    }
    addData(event) {
        const name = event.currentTarget.getAttribute('name');
        const dataToSend = {
            make: {
                url: 'api/makes',
                name: this.state.make,
                type: this.props.type,
            },
            body: {
                url: 'api/bodies',
                name: this.state.body,
                type: this.props.type,
            },
            modell: {
                url: 'api/models',
                name: this.state.modell,
                type: this.props.type,
                make_id: this.state.make_id,
            }
        }
        let formData = new FormData();
        for(let key in dataToSend[name]){
            if(key!=='url') formData.append(key, dataToSend[name][key]);
        }
        axios.post(dataToSend[name]['url'], formData, {
            headers: {
                'Authorization': 'Bearer '+this.props.user.auth_token,
            }
        }).then(json => {
            this.props.addData(name, json.data);
            this.setText(name, json.data.name, json.data.id); 
        });
    }
    submitPhotos(vehicle_id) {
        let promises=[];
        this.state.photos.map(photo => {
            let formData = new FormData();
            formData.append('path', photo.path);
            formData.append('vehicle_id', vehicle_id);
            promises.push(
                axios.post('api/photos', formData, {
                    headers: {
                        'Authorization': 'Bearer '+this.props.user.auth_token,
                        'Content-Type': 'multipart/form-data'
                    }
                }).then(json => {
                    
                }));
        });
        axios.all(promises).then(axios.spread((...args)=>{
            this.setState({
                uploaded:true,
                toHome:true,
                photos:[],
            });
        }));
    }
    
    render() {
        if(this.state.toHome) return <Redirect to='/'/>;
        
        const listaMarke = this.props.podaci.marke.filter((marka) => 
            this.props.type == marka.type &&
            (this.state.make 
                ? marka.name.toLowerCase().includes(this.state.make.toLowerCase())
                : true
            ));
        const listaKaroserije = this.props.podaci.karoserije.filter((karoserija) => 
            this.props.type == karoserija.type && 
            (this.state.body 
                ? karoserija.name.toLowerCase().includes(this.state.body.toLowerCase())
                : true
            ));
        const listaModeli = this.props.podaci.modeli.filter((model) => 
            this.state.make_id == model.make_id &&
            (this.state.modell
                ? model.name.toLowerCase().includes(this.state.modell.toLowerCase())
                : true
            ));
        const listaPogoni = this.props.podaci.pogoni.filter((pogon) =>
            (this.state.fuel 
                ? pogon.name.toLowerCase().includes(this.state.fuel.toLowerCase())
                : true
            ));
        const listaMjenjaci = this.props.podaci.mjenjaci.filter((mjenjac) =>
            (this.state.transmission 
                ? mjenjac.name.toLowerCase().includes(this.state.transmission.toLowerCase())
                : true
            ));

        let ispunjenaObavezna = true;
        obaveznaPolja.map(polje => ispunjenaObavezna = this.state[polje]);

        return (
            <div className='p-5 container bg-white shadow'>
                <div className='text-muted'>    
                    <TypeChooser size='h1' spacing='mx-4' handleTypeClick={this.props.handleTypeClick} type={this.props.type}/>
                </div>
                <h1 className='d-block text-center my-4'>Prodaj {vrsteVozilaSr[this.props.type]}</h1>
                <div className='row'>
                    <div className='col-sm-6'>
                        <div className='row'>
                            <div className='col-md-6'>
                                <Input 
                                    error={this.state.makeErr} 
                                    addData={this.addData} 
                                    setText={this.setText} 
                                    inputText={this.state.make} 
                                    caption='Marka*' 
                                    name='make'  
                                    lista={listaMarke}/>                       
                                <Input 
                                    error={this.state.bodyErr} 
                                    addData={this.addData} 
                                    setText={this.setText} 
                                    inputText={this.state.body} 
                                    caption='Karoserija*' 
                                    name='body' 
                                    lista={listaKaroserije}/>
                                <Input 
                                    error={this.state.transmissionErr} 
                                    setText={this.setText} 
                                    inputText={this.state.transmission} 
                                    caption='Mjenjač*' 
                                    name='transmission' 
                                    lista={listaMjenjaci}/>
                                <Input 
                                    error={this.state.priceErr} 
                                    unit='€' 
                                    setText={this.setText} 
                                    inputText={this.state.price} 
                                    caption='Cijena*' 
                                    name='price'/>
                                <Input 
                                    error={this.state.yearErr} 
                                    setText={this.setText} 
                                    inputText={this.state.year} 
                                    caption='Godište*' 
                                    name='year'/>
                                <Input 
                                    error={this.state.kmErr} 
                                    unit='km' setText={this.setText} 
                                    inputText={this.state.km} 
                                    caption='Kilometraža' 
                                    name='km'/>

                            </div>
                            <div className='col-md-6'>
                                <Input 
                                    error={this.state.modellErr} 
                                    addData={this.addData} 
                                    disabled={this.state.make_id ? false : true} 
                                    setText={this.setText} 
                                    inputText={this.state.modell} 
                                    caption='Model*' 
                                    name='modell' 
                                    lista={listaModeli}/>
                                <Input 
                                    error={this.state.fuelErr} 
                                    setText={this.setText} 
                                    inputText={this.state.fuel} 
                                    caption='Pogon*' 
                                    name='fuel' 
                                    lista={listaPogoni}/>
                                <Input 
                                    error={this.state.consumptionErr} 
                                    unit='l/100km' 
                                    setText={this.setText} 
                                    inputText={this.state.consumption} 
                                    caption='Potrošnja' 
                                    name='consumption'/> 
                                <Input 
                                    error={this.state.volumeErr} 
                                    unit='cm3' 
                                    setText={this.setText} 
                                    inputText={this.state.volume} 
                                    caption='Zapremina' 
                                    name='volume'/>
                                <Input 
                                    error={this.state.powerErr} 
                                    unit='KS' 
                                    setText={this.setText} 
                                    inputText={this.state.power} 
                                    caption='Snaga' 
                                    name='power'/>
                            </div>
                        </div>
                    </div>
                    <div className='col-sm-6'>
                        <AddPhotos 
                            handlePhotoClick={this.handlePhotoClick} 
                            photos={this.state.photos} 
                            uploadPhotos={this.uploadPhotos} />
                    </div>
                    <button
                        disabled={!ispunjenaObavezna} 
                        onClick={this.handleSubmit} 
                        className={'btn my-2 mx-auto px-4' + (ispunjenaObavezna?' btn-primary':' btn-secondary')}>
                            Prodaj
                    </button>
                </div>
            </div>
        );
    }
}
