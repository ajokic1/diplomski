<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

Route::group(['middleware' => ['jwt.auth','api-header']], function () {
    // all routes to protected resources are registered here  
    Route::get('users/list', function(){
        $users = App\User::all();
        
        $response = ['success'=>true, 'data'=>$users];
        return response()->json($response, 201);
    });

    Route::post('logout', 'UserController@logout');
    Route::get('me', 'UserController@me');
    Route::post('refresh', 'UserController@refresh');

    Route::post('bodies', 'BodyController@store');
    Route::post('makes', 'MakeController@store');
    Route::post('models', 'ModellController@store');

    Route::post('vehicles','VehicleController@store');
    Route::patch('vehicles/{vehicle}', 'VehicleController@update');
    Route::delete('vehicles/{vehicle}', 'VehicleController@destroy');

    Route::post('photos', 'PhotoController@store');
    Route::post('photos/temp', 'PhotoController@storeTemp');
    Route::post('photos/temp/delete', 'PhotoController@destroyTemp');
    Route::patch('photos/{photo}', 'PhotoController@update');
    Route::delete('photos/{photo}', 'PhotoController@destroy');
});
Route::group(['middleware' => 'api-header'], function () {
    Route::post('/login', 'UserController@login');
    Route::post('/register', 'UserController@register');
    
    Route::get('vehicles','VehicleController@index');
    Route::get('vehicles/{vehicle}','VehicleController@show');
    
    Route::get('models', 'ModellController@index');    
    Route::get('makes', 'MakeController@index');   
    Route::get('bodies', 'BodyController@index');

    Route::get('photos', 'PhotoController@index');
    Route::get('photos/{photo}', 'PhotoController@show');


});
