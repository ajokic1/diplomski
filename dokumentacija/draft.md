# Uvod

Web programiranje je danas jedan od najzastupljenijih oblika programiranja. U proteklim decenijama razvijene su mnoge tehnologije koje su omogućile kreiranje veoma kompleksnih web aplikacija koje sve više zamijenjuju klasični računarski softver. U ovom radu će biti predstavljen React.JS, izuzetno popularna JavaScript biblioteka za kreiranje korisničkih interfejsa, razvijena od strane Facebook-a. Kao uvod u priču o React-u, u nastavku će biti predstavljena kratka istorija razvoja web programiranja, kao i nekoliko ključnih tehnologija koje se koriste u svim web aplikacijama. Uprkos neuporedivoj kompleksnosti današnjih web aplikacija u odnosu na sajtove iz vremena kada je nastao web, osnova svake moderne web aplikacije su i dalje iste fundamentalne tehnologije.

## Istorija web programiranja

Može se reći da su osnove interneta kakav poznajemo danas nastale 1982. godine, kada je standardizovan skup protokola pod nazivom TCP/IP. Usvajanje ovog standarda omogućilo je međusobno povezivanje do tada izolovanih računarskih mreža i postepeno formiranje globalne mreže, ili "mreže svih mreža". Međutim, za masovnu upotrebu interneta, pored infrastrukture, bio je potreban i intuitivan sistem za pristupanje informacijama. Tim Berners-Lee, inženjer u CERN-u, razvio je ideju o globalnom informacionom sistemu u kome su dokumenti i drugi resursi povezani pomoću *hiperlinkova*. Ovaj sistem bi omogućio korisnicima da jednim klikom miša pristupe željenom sadržaju. Krajem 1989. godine, Berners-Lee je poslao svom menadžmentu predlog za sistem pod nazivom *World Wide Web*, koji bi učinio da internet postane stvarno korisan i pristupačan korisnicima. Do oktobra 1990., Berners-Lee je razvio tri ključne tehnologije koje su temelji današnjeg Web-a: 
- HTML - HyperText Markup Language - jezik za formatiranje dokumenata na web-u koji omogućava i povezivanje sa drugim dokumentima i resursima pomoću hiperlinkova
- URI - Uniform Resource Identifier - Jedinstvena adresa svakog resursa na web-u
- HTTP - Protokol nivoa aplikacije koji omogućava preuzimanje povezanih sadržaja sa web-a
Takođe, Berners-Lee je napisao i prvi web browser pod nazivom "WorldWideWeb", kao i prvi web server "httpd". Kako bi osigurao da ove tehnologije postanu globalni standard koji će biti implementiran na isti način u cijelom svijetu, Berners-Lee je 1994. osnovao W3C (World Wide Web Consortium) koji će definisati specifikacije i osigurati stabilan razvoj Web-a.
[webfundamentals, dart, cern]

Standardizacija, široka prihvaćenost i potpuna otvorenost fundamentalnih tehnologija web-a omogućile su njegov izuzetno brz razvoj. Internet je u potpunosti promijenio način na koji komuniciramo i razmijenjujemo podatke, a vremenom i sve druge aspekte ljudskog života. 

//U nastavku će biti ukratko predstavljene ključne tehnologije koje sačinjavaju osnovu svih modernih web sajtova, od najjednostavnijih web prezentacija do kompleksnih cloud aplikacija koje sve više zamijenjuju funkcionalnosti klasičnih desktop programa.

## HTTP
Svaka web aplikacija sastoji se iz dva osnovna dijela. U web browseru korisnika (klijentu) izvršava se *Front end*, tj. "prednji  dio" web sajta. Drugi dio, *Back end*, se izvršava na udaljenom računaru (serveru) i ima ulogu da obrađuje zahtijeve klijenta i šalje mu potrebne informacije. 

Komunikacija između klijenta i servera obavlja se pomoću *HTTP* protokola (*HyperText Transfer Protocol*), koji definiše dva tipa poruka - *zahtjev* i *odgovor*. 

Klijent šalje serveru *HTTP zahtjev* kako bi zatražio neku uslugu. Između ostalog, poruka zahtjeva sadrži i *metod* koji definiše vrstu usluge. Neki od često korišćenih metoda su: 
- *GET* (klijent zahtijeva neke podatke od servera)
- *POST* (klijent šalje podatak serveru za čuvanje) 
- *DELETE* (klijent zahtijeva brisanje podataka sa servera)

Nakon prijema i obrade zahtjeva, server šalje *HTTP odgovor*. Poruka odgovora sadrži *statusni kod* koji izvještava klijenta o uspješnosti izvršenja tražene usluge. Neki od ovih kodova su:
- 200 OK - zahtjev je uspješno izvršen, traženi podaci se nalaze u tijelu poruke
- 404 Not Found - traženi resurs nije pronađen
- 403 Forbidden  - nemate dozvolu za pristup traženom resursu

## HTML
HTML (*HyperText Markup Language*) je jezik za definisanje strukture web stranice. Dokument napisan u HTML-u sastoji se iz *elemenata*, koji su ograničeni *tagovima*. Tagovi označavaju vrstu elementa i definišu njegovo ponašanje i ulogu u dokumentu. Na primjer, `<title>` tag označava naslov dokumenta, `<p>` označava paragraf, dok se `<div>` koristi za grupisanje sadržaja. Pored naziva, u tagu se mogu definisati i atributi, pomoću kojih se modifikuju određena svojstva elementa. Pokazalo se da je HTML veoma efikasan način za definisanje strukture web stranice i koristi se u svim web sajtovima od nastanka web-a do danas.
[w3]

## CSS
HTML je pogodan za kreiranje sadržaja i strukture stranice, ali ima veoma ograničene mogućnosti promjene izgleda samih elemenata. Na primjer, promjena boje teksta svih podnaslova na stranici bi podrazumijevala promjenu atributa svakog elementa pojedinačno, što je bilo veoma nepraktično za razvoj velikih web sajtova. Kao rješenje ovog problema predstavljen je CSS (*Cascading Style Sheets*), jezik za definisanje izgleda web stranice i njenih elemenata. CSS fajl se sastoji iz skupa "pravila" koja se primjenjuju na određene elemente stranice. Jedno "pravilo" u CSS-u sačinjavaju *selektori* i skup *svojstava*. Pomoću selektora se određuje koji će elementi biti zahvaćeni ovim pravilom, dok svojstva definišu osobine odabranih elemenata koje će biti promijenjene. Jedan primjer CSS pravila je:
```CSS
p {
    color: red;
}
```
Ovdje, `p` predstavlja selektor, koji bira sve elemente na stranici koji su ograničeni `<p>` tagovima, dok svojstvo `color: red;` postavlja boju teksta svih odabranih elemenata na crvenu. Na ovaj način, postalo je moguće mijenjati izgled velikog broja elemenata sa jednim pravilom, kao i definisanje izgleda cijelog web sajta na jednom mjestu. 

## JavaScript
Sa razvojem web tehnologija, poboljšavao se izgled stranica i sajtovi su postajali sve kompleksniji. Međutim, stranicama je nedostajala još jedna stvar - *interaktivnost*. Hiperlinkovi su omogućavali jednostavan prelazak na druge stranice jednim klikom. Međutim, šta ako želimo da klikom na neko dugme promijenimo neki dio stranice, ili da se neki podatak na stranici ažurira bez osvježavanja stranice? Za ove funkcionalnosti bio je potreban pravi programski jezik koji bi imao pristup elementima web stranice. Postojalo je nekoliko pokušaja uvođenja programskih jezika u web browsere, ali ni jedan od njih nije doživio veliki uspjeh sve do pojave *JavaScript*-a.
[dart] 

JavaScript je razvijen od strane Brendana Eicha, zaposlenog u Netscape-u ({footnote}kompaniji koja je razvila popularni browser *Netscape Navigator*). Uprkos imenu, JavaScript nije baziran na programskom jeziku Java, niti je imao nekih značajnih sličnosti sa ovim jezikom. Naziv mu je dat čisto radi marketinga, kako bi se iskoristila tadašnja izuzetna popularnost Jave. Standardizovan je pod nazivom *ECMAScript*, i trenutna najnovija verzija je ECMAScript 2015 (poznat i kao ECMAScript 6 ili ES6). JavaScript je veoma brzo dobijao na popularnosti, ali je u početku korišćen samo za veoma male zadatke, najviše zbog nedostatka brzine. Takođe, postojao je i problem kompatibilnosti među *browser*-ima, prvenstveno u Internet Explorer-u, čija implementacija JavaScripta nije bila u skladu sa standardima. Vremenom je brzina rasla, funkcionalnost se širila, a poboljšala se i kompatibilnost.  
[Dart]

Sa tehničke strane, JavaScript je laki, interpretirani, dinamični, objektno-orjentisani (počev od ES6) jezik sa funkcijama prve klase
[https://en.wikipedia.org/wiki/First-class_function]. JavaScript podržava objektno-orjentisani, imperativni i funkcionalni stil programiranja. Jedna veoma značajna karakteristika JavaScript-a je da sve predstavlja objekat - primitivni tipovi, nizovi, pa čak i funkcije su objekti. Takođe, JavaScript ima *prototipski* način kreiranja objekata. Ovo podrazumijeva da se ne pravi razlika između *klasa* i *instanci*, već se novi objekti prave kloniranjem postojećih "šablonskih" objekata (prototipa). 
[MDN - About JS]

Danas je JavaScript osnovni jezik web-a i gotovo da ne postoji ozbiljan web sajt koji ga ne kao glavni jezik na front end-u. Prvenstveno se koristi na klijentskoj strani, ali u poslednje vrijeme postaje popularan i kao *back-end* jezik (Node.JS). Razvoj JavaScript-a je omogućio pojavu tzv. web aplikacija, tj. web sajtovi više ne služe samo za prikaz informacija već dobijaju funkcionalnosti slične klasičnim "desktop" programima. Popularnost web aplikacija je veoma brzo rasla jer je omogućila kreiranje softvera kome se može pristupiti sa svakog računara koji ima internet konekciju, bez potrebe za instalacijom. 

### DOM
Kako bi JavaScript unio dinamičnost u web stranice, potreban je neki način da se programski pristupi elementima web stranice. U tu svrhu kreiran je DOM (*Document Object Model*), interfejs koji omogućava standardizovani način pristupa i dinamičkog ažuriranja HTML dokumenta. U DOM-u, HTML dokument je modelovan stablom koje sadrži sve elemente na stranici. Elementi su u programskom jeziku modelovani objektima koji sadrže sve HTML atribute, sadržaj elementa, kao i metode i događaje koji su relevantni za taj element. Na ovaj način, programer može iz JavaScripta jednostavno pristupiti svakom dijelu stranice i manipulisati elementima kao sa svakim drugim JavaScript objektom. Zadatak web browser-a je da se stranica koju vidi korisnik u realnom vremenu sinhronizuje sa DOM-om. Efikasna manipulacija DOM-om je ključni koncept u razvoju dinamičnih web aplikacija i često ima veliki uticaj na performanse aplikacije.
[MDN, W3]

### AJAX
Kao što je pomenuto, komunikacija između servera i klijenta obavlja se pomoću HTTP poruka. Međutim, kod klasičnih web stranica se podaci preuzimaju sa servera samo prilikom učitavanja stranice. Ako se podaci na serveru promijene, korisnik će vidjeti nove podatke tek nakon ponovnog učitavanja (osvježavanja) stranice. Kod aplikacija koje rade sa brzo promjenljivim podacima ovaj način osvježavanja podataka nije pogodan. Rješenje ovog problema je jednostavna tehnika poznata pod nazivom AJAX (*Asynchronous JavaScript And XML*). AJAX podrazumijeva kreiranje i slanje HTTP zahtjeva serveru pomoću JavaScripta i izvršavanje neke akcije nakon prijema odgovora, pri čemu se sve ovo izvršava u pozadini, bez osvježavanja stranice. Na primjer, ukoliko želimo da napravimo stranicu koja prikazuje neki podatak sa servera u realnom vremenu, to možemo uraditi na sljedeći način: Prvo u JavaScriptu podesimo tajmer koji će u određenim vremenskim intervalima slati zahtjeve koji traže odgovarajući podatak od servera. Zatim definišemo obrađivač događaja koji će nakon prijema HTTP odgovora modifikovati odgovarajući DOM objekat kako bi se na stranici prikazali novi podaci. Ova tehnika se koristi u mnogim današnjim web sajtovima i takođe je gotovo nezaobilazna u kreiranju modernih web aplikacija.
[MDN, W3]

## JavaScript biblioteke i framework-ovi
Kako je web tehnologija napredovala, JavaScript se sve više koristio u web sajtovima, kako za dodavanje novih kompleksnih funkcionalnosti, tako i za poboljšavanje korisničkog iskustva (npr. validacija formi u realnom vremenu, auto-complete, lazy loading...). Sve ove funkcionalnosti zahtijevaju ogromnu količinu koda u JavaScriptu. Međutim, kako mnogi sajtovi koriste gotovo iste osnovne funkcionalnosti, ispostavlja se da je u svakom novom projektu potrebno iznova pisati veliku količinu istog koda. Zbog toga su razvijene mnoge biblioteke i framework-ovi koji sadrže gotove implementacije funkcionalnosti koje se često koriste. 

Riječi "framework" i "biblioteka" se često koriste kao sinonimi. Oba termina označavaju neki gotovi programski kod koji olakšava implementaciju nekih uobičajenih funkcionalnosti. Međutim, postoji jedna ključna razlika između ova dva termina koju je bitno razumjeti. 

Biblioteka se najčešće sastoji iz definicija klasa ili funkcija koje programer može koristiti u svom projektu. Može se opisati kao skup "alata" koji se koriste za postizanje željene funkcionalnosti. Pri tome, biblioteke pružaju programeru potpunu slobodu u pogledu strukture ostatka njegovog koda. Primjer biblioteke je jQuery, koja omogućava kraći i intuitivniji zapis prilikom pristupanja elementima DOM-a, kao i olakšanu obradu događaja, animaciju i upotrebu AJAX-a.

Framework, sa druge strane, ima definisanu strukturu koju programer mora poštovati i mora se uklopiti u nju. Ovaj koncept se često naziva "inverzija kontrole". Naime, kada koristi biblioteku, programer je u kontroli, i poziva funkcije biblioteke kada su mu potrebne. Sa framework-om imamo suprotnu situaciju - struktura programa već postoji, a u njoj postoje određene "praznine" u koje programer treba da unese svoj kod. Dok ovaj pristup smanjuje nivo slobode programera, prednost je u tome što ne mora da razmišlja o organizaciji i dizajnu koda već samo da implementira funkcionalnosti koje su mu potrebne poštujući predefinisanu strukturu koda.
https://www.freecodecamp.org/news/the-difference-between-a-framework-and-a-library-bd133054023f/
https://www.atlantic.net/cloud-hosting/what-is-javascript-frameworks-introduction/

### Single-page aplikacije
Single-page aplikacija (SPA) predstavlja web aplikaciju koja se izvršava na samo jednoj web stranici, bez potrebe za učitavanjem novih stranica. Pri tome, sva interakcija sa korisnikom, komunikacija sa serverom i sve promjene promjene stranice koja se prikazuje korisniku obavljaju se dinamički, pomoću JavaScript-a. 

Ovaj pristup donosi mnoge prednosti. Jedna od njih je poboljšanje korisničkog iskustva - rad sa aplikacijom je "tečniji" zato što nema prekida i čekanja prilikom učitavanja nove stranice. Zatim, aplikacija generalno radi brže zato što se pri prvom učitavanju stranice učita veliki dio potrebnih resursa (HTML, CSS, sktripte), a u toku rada se sa serverom samo razmjenjuju male količine podataka. Takođe, kod za renderovanje stranica prelazi sa servera na klijent, što čini back-end kod jednostavnijim i smanjuje opterećenje servera. Ovo znatno olakšava i kreiranje mobilnih aplikacija jer web i mobilna verzija mogu da dijele isti back-end. Naravno, SPA pristup ima i svoje mane, kao što je otežana optimizacija za pretraživače (SEO), veće opterećenje klijenta i neophodnost JavaScripta za rad stranice. 

Ovaj pristup je danas veoma popularan i koriste ga mnoge poznate aplikacije kao što su Gmail, Google Maps, Facebook itd. Mnogi moderni framework-ovi i biblioteke su prihvatili ovaj pristup i značajno olakšavaju kreiranje ovakvih aplikacija. Vremenom su razvijena i rješenja za neke od mana ovog pristupa, npr. rutiranje omogućava vezivanje stanja aplikacije za URL, nedostatak JavaScript-a u browseru se može detektovati i prikazati klasična verzija sajta, Google radi na optimizaciji svog pretraživača za ovakve aplikacije itd.

### MVC
MVC (skraćeno od Model, View, Controller) je obrazac dizajna korisničkih interfejsa u kome se aplikacija razdvaja na tri međusobno povezana dijela:
- Model obuhvata sve podatke sa kojima radi aplikacija
- Pregled (view) obuhvata sve ono što se prikazuje korisniku na ekranu
- Kontroler obuhvata svu logiku obrade korisnikovih zahtjeva i razmjenjuje odgovarajuće podatke sa modelom i pregledom

Ovaj princip je danas široko prihvaćen kao standardna arhitektura web aplikacija i mnogi framework-ovi su bazirani na njemu.

# React.JS
React je razvijen 2013. godine od strane Facebook-a. Prema riječima njegovih autora, React je deklarativna, efikasna i fleksibilna JavaScript biblioteka za izgradnju korisničkih interfejsa. Analizirajmo ove tri osobine malo detaljnije:
- *Deklarativna*: Deklarativno programiranje se može opisati kao stil programiranja u kome programer definiše *šta* program treba da radi a ne *kako* to treba da uradi. U React-u ovo znači da mi nikada ne interagujemo direktno sa DOM-om, već samo mijenjamo stanje komponenti, dok se React brine o ažuriranju DOM-a u skladu sa ovim promjenama. 
- *Fleksibilna*: Za razliku od velikih framework-ova kao što je Angular, u kome je tačno definisan način na koji se pojedine stvari implementiraju, React je samo biblioteka koja olakšava programeru kreiranje kompleksnih interfejsa onako kako želi. Pri tome, programer je potpuno slobodan da koristi React u proizvoljnoj mjeri, tj. može uključiti samo pojedine React komponente u postojeću stranicu, a može i kreirati single-page aplikaciju koja je u potpunosti napisana u React-u. 
- *Efikasna*: React ima tzv. *virtuelni DOM*, koji u slučaju promjene nekog podatka upoređuje trenutno i prethodno stanje stranice i samo obrađuje promjene, tj. mijenja samo onaj dio stranice koji je potrebno promijeniti. Ovo je omogućilo ogromno povećanje performansi kod kompleksnih aplikacija i predstavljalo je ključnu funkcionalnost koja je stavila React ispred svoje konkurencije. 

Za razliku od kompleksnih SPA framework-ova, koji nam u startu pružaju sve što je potrebno za našu aplikaciju, React se koncentriše samo na sloj pregleda[footer], tj. pruža nam samo "V" u MVC obrascu. Ovo nam omogućava da se skoncentrišemo na kreiranje izgleda korisničkog interfejsa, a zatim da postepeno dodajemo druge aspekte aplikacije. Osnovna ideja React-a je da pregled predstavlja hijerarhiju malih izolovanih komponenti koje se mogu sastavljati. Prema tome, aplikaciju možemo kreirati korak po korak: prvo kreiramo neophodne komponente, a zatim ih iskoristimo kao gradivne blokove za izgradnju naše aplikacije.
//Kao što je pomenuto, React nije SPA framework već biblioteka, ili preciznije, biblioteka pregleda. On nam pruža samo "V" u MVC obrascu, dok nam brojne druge biblioteke iz React ekosistema omogućavaju kreiranje potpunih SPA aplikacija. Ovo nam omogućava da se skoncentrišemo na kreiranje pregleda, a zatim da postepeno dodajemo druge aspekte aplikacije. //ovo može kod opisa ekosistema

//Osnovna ideja React-a je da pregled predstavlja hijerarhiju malih komponenti koje se mogu sastavljati. Komponente bi trebale da obavljaju samo jednu stvar i da budu nezavisne od konteksta. Na ovaj način, možemo prvo kreirati sve komponente zasebno, bez razmišljanja o kompletnoj aplikaciji, a zatim da ove komponente iskoristimo kao gotove blokove za izgradnju aplikacije. 

FOOTER[pregled (view) - skup vizuelnih elemenata koji sačinjavaju korisnički interfejs (ili njegov dio)]

## Zašto react?
React je danas najpopularnija front-end tehnologija, dok drugo i treće mjesto zauzimaju Angular i Vue, respektivno. Na slici je prikazan grafik koji predstavlja mjesečni broj preuzimanja odgovarajućih paketa pomoću *npm*-a u poslednje tri godine. 
[grafik]
[https://npm-stat.com/charts.html?package=react&package=vue&package=%40angular%2Fcore&from=2016-12-12&to=2019-06-29]

Angular je najstarija od ove tri tehnologije. Razvijen je 2012. godine od strane Google-a i veoma brzo je dobijao na popularnosti. Angular je kompletni MVC framework sa jednostavnim two-way data binding mehanizmom, ugrađenim sistemom za rutiranje i modularnim dizajnom. Predefinisana struktura aplikacije u Angular-u olakšava razvoj velikih aplikacija i forsira programera da se drži pravila koja vode ka razvoju održivog softvera. Međutim, ovo prirodno vodi smanjenju fleksibilnosti. Takođe, kriva učenja je veoma strma i programerima je potrebno mnogo vremena da se prilagode ovom framework-u.

Kada je razvijen React, imao je nekoliko ključnih prednosti u odnosu na Angular. Njegov *virtuelni DOM* je donio značajno poboljšanje performansi. Fokusiranost na izgradnju korisničkih interfejsa i olakšavanje manipulacije DOM-a bez forsiranja neke predefinisane strukture aplikacije donosi mnogo veću fleksibilnost. Pri tome, koncepti na kojima je zasnovan React su usmjereni na kreiranje preglednog, održivog i proširivog koda što ga čini pogodnim za razvoj izuzetno kompleksnih aplikacija. Međutim, implementacija pojedinih funkcionalnosti je ponekad poprilično kompleksna i često zahtijeva pisanje velike količine naizgled "nepotrebnog" koda. Takođe, dok je znatno jednostavniji za učenje od Angular-a, i dalje je za razvoj u React-u potrebno dosta učenja, kao i dobro poznavanje JavaScript-a.

Vue je nastao 2014. godine i jedini je od ove tri tehnologije koji je razvijen bez podrške neke velike kompanije. Razvijen je sa ciljem da sastavi prednosti React-a i Angular-a i oslobodi se nekih od njihovih mana. Veoma je brzo dobijao na popularnosti, a po nekim mjerilima je čak prevazišao React (trenutno ima oko 142 000 "zvjezdica" na github-u, dok React ima oko 131 000). Usmjeren je na jednostavnost implementacije (sa što manje koda i velikom slobodom), izuzetno malu veličinu i brzinu (trenutno je najbrži, dok je React na drugom mjestu po brzini). Takođe, osnove Vue-a se mogu veoma brzo naučiti, sa postepenim usvajanjem naprednijih tehnika. Međutim, dok ova jednostavnost značajno ubrzava razvoj manjih aplikacija, kod kompleksnih aplikacija ovo dovodi do težeg održavanja i više mogućnosti za greške. 

Dakle, React je dizajniran za razvoj velikih, kompleksnih aplikacija pružajući pri tome potpunu slobodu i fleksibilnost, kao i veliku brzinu. Dok je implementacija nekih funkcionalnosti kompleksnija nego u Vue-u, nakon prolaska kroz ključne koncepte React-a ćemo vidjeti kako ova kompleksnost doprinosi kreiranju preglednog i održivog koda. React takođe ima veoma kvalitetnu dokumentaciju, a njegova popularnost donosi i veliku podršku zajednice. 

## Ključni koncepti

### JSX
*JSX* je sintaksa koja se koristi u React-u i predstavlja specifičnu kombinaciju JavaScript-a i HTML-a. Jedan jednostavan primjer JSX koda izgleda ovako:

```
const name = "Andrej";
const element = <h1>Hello, {name}!</h1>

ReactDOM.render(
    element,
    document.getElementById('root')
);
```
Nakon izvršavanja ovog koda, React će u HTML stranici pronaći element sa atributom `id="root"`, i u njega će dodati element `<h1>Hello, Andrej!</h1>`. JSX izraz `<h1>Hello, {name}!</h1>` će u toku izvršavanja postati standardni JavaScript objekat, što nam omogućava da ga jednostavno dodijelimo promjenljivoj i koristimo dalje u kodu. 

U JSX izraz možemo uključiti bilo koju JavaScript promjenljivu korišćenjem vitičastih zagrada. Pri tome, nije potrebno voditi računa o *injection* napadima[footer], jer React svaku vrijednost uključenu na ovaj način escape-uje[footer].

JSX mnogim iskusnim web programerima djeluje neprirodno jer je na jednom mjestu "pomiješana" logika i izgled stranice. Ovo se kosi sa ustaljenom praksom u web programiranju koja podrazumijeva razdvajanje *tehnologija*, tj. HTML (koji definiše izgled) i JavaScript (koji sadrži logiku) se pišu u zasebnim fajlovima. Međutim, činjenica je da je logika prikaza usko povezana sa ostalom logikom korisničkog interfejsa. Ideja React-a je da se, umjesto vještačkog razdvajanja *tehnologija*, razdvoje samo oni djelovi programa koji rade različite stvari. Drugim riječima, cilj je da se sva logika vezana za određenu funkcionalnost programa nalazi na jednom mjestu. Ovo se postiže grupisanjem koda u *komponente* koje će biti detaljnije objašnjene u narednim poglavljima.

Prednost React-ovog pristupa može se ilustrovati sljedećim primjerom: Pretpostavimo da imamo web stranicu kojom upravlja određena front-end logika napisana u JavaScript-u. Zatim, pretpostavimo da je potrebno promijeniti neki dio te stranice u HTML-u. Kako je u JavaScript-u taj dio stranice referenciran pomoću DOM-a, svaka promjena DOM-a može promijeniti i ponašanje JavaScript logike. Prema tome, nakon svake izmjene u jednom fajlu moramo provjeriti i sve ostale fajlove koji referenciraju taj dio DOM-a kako bi osigurali da stranica i dalje radi kako je očekivano. U kompleksim projektima ovo značajno otežava održavanje i proširivanje koda, jer pri svakoj promjeni moramo modifikovati veliki broj fajlova, iako u suštini modifikujemo samo jednu funkcionalnost aplikacije. U React-ovom pristupu, sa druge strane, sav kod koji je vezan za jednu funkcionalnost grupisan je u okviru jedne komponente. 

JSX takođe olakšava pristup elementima stranice jer su ??? definisani pomoću JSX pa ne trebaju DOM selektori???



    Reduce coupling increase cohesion
    cohesion - degree to which modules rely on other modules - kada ispravljamo bug, u koliko odvojenih djelova koda moramo napraviti izmjene da bi to funkcionisalo

### Elementi
Kao što je rečeno, JSX izrazi se u toku izvršavanja prevode u JavaScript objekte. Ovakav objekat se u React-u naziva *elementom* i predstavlja najmanju gradivnu jedinicu React aplikacije. React elementi su nepromjenljivi, tj. nakon kreiranja elementa ne možemo promijeniti ni njegove osobine ni njegov sadržaj. Jedini način da promijenimo neki element je da kreiramo novi i zamijenimo ga. Nakon zamjene React neće mijenjati sve djelove DOM-a koji su definisani tim elementom, već će uporediti stari i novi element i samo promijeniti onaj dio koda koji je zaista promijenjen. 

Da bi neki element prikazali na stranici, potrebno je da u HTML-u kreiramo tzv. korijeni (*root*) DOM čvor. Ovaj HTML element će sadržati samo React elemente, tj. React će upravljati cjelokupnim sadržajem tog elementa. Zatim, potrebno je da pozovemo funkciju `ReactDOM.render()` i proslijedimo joj element koji želimo renderovati, kao i odgovarajući *root* element:

```JavaScript
ReactDOM.render(element, document.getElementById('root'));
```

Najčešće se u React-u prave single-page aplikacije koje imaju samo jedan korijeni čvor. Tada React u toku izvršavanja stranice dinamički mijenja sadržaj korijenog čvora. Međutim, važno je napomenuti da ovakav pristup nije obavezan. Naime, u aplikaciji možemo imati i više korijenih čvorova, koji će sadržati različite React elemente. Ovo je veoma korisno u situaciji kada imamo postojeću stranicu koju želimo nadograditi sa pojedinim elementima iz React-a, bez potrebe da se kompletna stranica prevodi u React.

### Komponente
Komponente su osnovni "gradivni blokovi" React aplikacije. Dok element definiše izgled jednog dijela korisničkog interfejsa u određenom trenutku, komponenta obuhvata svu logiku koja definiše jednu funkcionalnu cjelinu aplikacije. Komponenta se može deklarisati bilo kao JavaScript funkcija ili kao klasa, i može da primi ulazne podatke koji se nazivaju svojstvima (props). Kao rezultat, komponenta vraća React element koji opisuje kako izgleda određeni dio korisničkog interfejsa u određenom trenutku vremena. U slučaju promjene podataka, komponenta generiše novi element. 

```JavaScript
//Komponenta deklarisana kao funkcija
function Pozdrav(props) {
  return <h1>Hello, {props.name}</h1>;
}

//Komponenta deklarisana kao klasa
class Pozdrav {
    render() {
        return <h1>Hello, {this.props.ime}</h1>;
    }
}
```

Komponenta se može prikazati na stranici pomoću JSX-a. Naime, pored standardnih HTML tagova, za kreiranje elemenata u JSX-u možemo na isti način koristiti i komponente koje smo prethodno definisali. Element se shodno tome može posmatrati i kao "instanca" komponente. Nazivi komponenti se u JSX-u uvijek pišu velikim početnim slovom, dok tagove koji počinju malim slovom React interpretira kao HTML tagove. 

```JavaScript
function Aplikacija(props) {
    return <Pozdrav ime="Andrej"/>;
}

ReactDOM.render(
    <Aplikacija />, 
    document.getElementById('root')
);
```

U ovom primjeru smo kreirali komponentu `Aplikacija` koja vraća element komponente `Pozdrav` sa svojstvom `ime`. Zatim smo komponentu `Aplikacija` prikazali u korijenom DOM čvoru. Hijerarhijskim sastavljanjem komponenti na ovaj način možemo kreirati proizvoljno kompleksnu React aplikaciju. Single-page aplikacije najčešće imaju jednu komponentu koja se renderuje u DOM-u, a sve ostale komponente se nalaze unutar nje. 

Prilikom kreiranja komponenti bitno je voditi računa o njihovoj univerzalnosti. Ideja React-a je da se jedna ista komponenta može koristiti na više mjesta u aplikaciji. Zbog toga, poželjno je da se komponente ponašaju na isti način bez obzira na kontekst u kome se koriste. Takođe, komponente bi trebale da budu što jednostavnije, tj. svaka bi trebala da obavlja samo jednu funkcionalnost. Ovakav pristup nam omogućava da aplikaciju izdijelimo na veliki broj jednostavnih, izolovanih djelova koje možemo razvijati zasebno, a zatim sastaviti u funkcionalnu cjelinu bez suvišnog ponavljanja koda. Ovo znatno olakšava održavanje i proširivanje koda kod velikih aplikacija.

Razdvajanje djelova aplikacije na manje komponente neizbježno povećava količinu koda koji je potrebno napisati u početku. Međutim, sa povećanjem kompleksnosti aplikacije, ovaj pristup postaje sve isplativiji jer omogućava korišćenje jednom napisanog koda na različitim mjestima. Ovo čini React pogodnim za razvoj izuzetno kompleksnih i velikih aplikacija. Jednostavan način da odredimo da li neku funkcionalnost treba izdvojiti u zasebnu komponentu je da razmislimo da li se ona može iskoristiti još negdje u aplikaciji. Pored toga, ponekad je dobro izdvojiti neku funkcionalnost u zasebnu komponentu kada kod postane suviše kompleksan, radi povećanja preglednosti

Univerzalnost komponenti se može ilustrovati na primjeru "Like" dugmeta na Facebook-u. Ovo dugme se nalazi na svakoj objavi, komentaru i odgovoru na komentar. Pri tome, svako dugme se ponaša na isti način - prelaskom kursora preko njega pojave se dostupne "reakcije", a klikom na neku od njih korisnik "reaguje" na odgovarajuću objavu ili komentar. Ovo dugme je poželjno definisati kao zasebnu, nezavisnu komponentu koja se može koristiti i za objave i za komentare. Na ovaj način, za promjenu ponašanja ili izgleda sistema za reakcije potrebno je samo promijeniti odgovarajuću komponentu.

### Svojstva (props)
Svojstva su osnovni način za razmjenu podataka između komponenti. Kada deklarišemo React element kao instancu komponente, možemo mu proslijediti proizvoljan broj svojstava. Svojstvima se može proslijediti bilo kakav JavaScript objekat i definišu se na isti način kao HTML atributi:

```xml
<Komentar
    korisnik={korisnik}
    tekst="Neki komentar"
/>
```

Komponenta može pristupiti svojstvima koja su joj proslijeđena pomoću `this.props` (ili samo `props` ukoliko je deklarisana kao funkcija). 

```JavaScript
function Komentar(props)
    return (
        <div>
            <strong>{props.korisnik.ime}</strong>
            <p>{props.tekst}</p>
        </div>
    );
```

Iako je React poprilično fleksibilan, jedno pravilo se uvijek mora poštovati: sva svojstva su *read-only*, tj. komponenta nikada ne smije modifikovati svoja svojstva. Drugim riječima, komponenta treba da se ponaša kao zatvoren sistem, koji za određene ulazne podatke uvijek vraća iste izlazne podatke, bez ikakvog uticaja na okolinu. Ovo nam omogućava da razmišljamo o komponenti kao o izolovanom, nezavisnom gradivnom elementu koji se može koristiti bilo gdje u aplikaciji. U programiranju se funkcije koje ne modifikuju svoje ulazne podatke nazivaju *čistim* funkcijama (eng. *pure function*).

### Stanje (state)
U prethodnom odjeljku je pomenuto da su svojstva nepromjenljiva. React je biblioteka za kreiranje dinamičnih aplikacija koje rade sa podacima koji se mijenjaju. Iz ovog razloga se uvodi koncept *stanja* (state), koje sadrži sve promjenljive podatke sa kojima komponenta radi. Stanje je izolovano na unutrašnjost komponente tj. ne može mu se pristupiti spolja. Važno je napomenuti da stanje mogu imati samo komponente koje su deklarisane pomoću klase. Kao primjer, napravimo jednostavnu komponentu sa brojačem koji će se uvećavati pri svakom kliku na dugme. Početno stanje se definiše u konstruktoru komponente, a pristupa mu se preko `this.state`:

```JavaScript
class Brojac extends React.Component {
    constructor(props) {
        super(props);
        this.state={
            brKlikova=0;
        }
    }
    render() {
        return (
            <p>Broj klikova: {this.state.brKlikova}</p>
            <button>Klikni ovdje</button>
        );
    }
}
```

Postavlja se pitanje koja je razlika između stanja i običnih JavaScript promjenljivih. Naime, promjena stanja se ne vrši direktno, već pomoću funkcije `setState()` . Pored promjene odgovarajućeg stanja, ova funkcija obavještava React da je stanje promijenjeno i da se odgovarajući elementi moraju osvježiti. Ako u elementu koristimo obične promjenljive ili dodijelimo stanje direktno (npr `this.state.brKlikova++;`), React neće znati kada se promijenila njihova vrijednost i elementi na stranici se neće ažurirati. Dopunimo sada prethodni primjer sa obradom klika na dugme:

```JavaScript
class Brojac extends React.Component {
    constructor(props) {
        super(props);
        this.state={
            brKlikova=0;
        }
    }
    handleClick() {
        this.setState((state, props) => ({
            brKlikova: state.brKlikova+1
        }));
    }
    render() {
        return (
            <p>Broj klikova: {this.state.brKlikova}</p>
            <button onClick={this.handleClick}>Klikni ovdje</button>
        );
    }
}
```

Obratimo pažnju na argument funkcije `setState()` unutar funkcije `handleClick()`. Zašto nismo napisali jednostavno `this.setState({brKlikova: this.state.brKlikova + 1})`? Naime, radi poboljšanja performansi, React ažuriranje stanja može izvršavati asinhrono, tj. može sastaviti nekoliko `setState` poziva u jedan. Zbog ovoga, prilikom ažuriranja stanja se ne smijemo oslanjati na njegove trenutne vrijednosti za izračunavanje novog stanja. Umjesto toga, funkciji `setState()` proslijeđujemo anonimnu funkciju koja prima trenutne vrijednosti stanja kao argumente, a unutar nje izračunavamo novu vrijednost stanja. Na ovaj način, funkcija će primiti vrijednost u trenutku ažuriranja stanja i radiće na očekivan način. 

### Zivotni ciklus komponente
Svaka komponenta ima niz metoda koje se pozivaju u određenom redosljedu od njenog prvog prikaza na ekranu do njenog uklanjanja. Ovaj niz naziva se "životni ciklus" komponente. Zivotni ciklus se može podijeliti na tri faze: 
- Mounting - element komponente se postavlja na stranicu
- Updating - element se ažurira sa novim podacima
- Unmounting - element se uklanja sa stranice

Metode životnog ciklusa su korisne za implementaciju akcija koje je potrebno izvršiti u određenoj fazi postojanja komponente. Jedna od značajnijih metoda je `componentDidMount()` koja se poziva nakon što se element prvi put prikazao na stranici. Ova metoda je pogodna za npr. slanje AJAX zahtijeva za preuzimanje podataka ili startovanje tajmera. `componentWillUnmount()` se poziva prije uklanjanja komponente i korisna je za oslobađanje resursa koje je komponenta zauzela. `componentDidUpdate()` se poziva odmah nakon ažuriranja (a prije prikaza novog elementa na stranici) i obično se koristi za modifikaciju DOM-a ili preuzimanje podataka sa servera ukoliko se neko svojstvo promijenilo (mora se provjeriti da li se svojstvo promijenilo inače dolazi do beskonačne petlje). Postoji još metoda životnog ciklusa ali se ostale rijetko koriste. Na slici je prikazan dijagram životnog ciklusa. Neke od metoda su u novijim verzijama React-a označene sa `UNSAFE` jer mogu da izazovu neočekivano ponašanje i greške pa se ne preporučuje njihova upotreba. 

iz pure react - ima i slika

### Tok podataka među komponentama
U React-u, stanje je ograničeno na unutrašnjost komponente, a komunikacija među komponentama se obavlja pomoću svojstava. Svojstva potiču od roditeljske komponente i ne mogu se modifikovati. Prema tome, u React-u podaci mogu da teku samo u jednom smjeru - od vrha ka dnu. Svaka komponenta može uticati samo na komponente koje se nalaze ispod nje u hijerarhiji. 

Postavlja se pitanje na koji način se može ostvariti komunikacija u suprotnom smjeru, tj. kako komponenta može poslati podatke svojim roditeljima. Mnogi framework-ovi imaju mehanizam pod nazivom "two-way data binding" koji omogućava dvosmjerni tok podataka. Na primjer, u Angular-u komponente mogu da modifikuju podatke koje dobijaju od roditelja. U React-u ovo nije moguće jer su svojstva *read-only*. Da bi komponenta poslala podatke roditelju, potrebno je da joj roditelj preko svojstava proslijedi funkciju. Komponenta će tada pozvati tu funkciju i preko argumenata joj proslijediti odgovarajuće podatke. Konačno, u roditeljskoj komponenti će se ta funkcija izvršiti i obraditi primljene podatke. Ovaj koncept se može pokazati na brojaču iz prethodnog primjera ako se dugme izdvoji u zasebnu komponentu:

```JavaScript
class Brojac extends React.Component {
    constructor(props) {
        super(props);
        this.state={
            brKlikova=0;
        }
        this.handleClick=this.handleClick.bind(this);
    }
    handleClick() {
        this.setState((state, props) => ({
            brKlikova: state.brKlikova+1
        }));
    }
    render() {
        return (
            <p>Broj klikova: {this.state.brKlikova}</p>
            <Dugme onClick={this.handleClick} />
        );
    }
}
class Dugme extends React.Component {
    render(
        <button onclick={this.props.handleClick}>Klikni ovdje</button>
    );
}
```

Kada korisnik klikne na `button` u komponenti `Dugme`, poziva se funkcija `handleClick` koja se nalazi u roditeljskoj komponenti i koja mijenja njeno stanje. Obratimo pažnju na konstruktor brojača, u koji smo dodali `this.handleClick = this.handleClick.bind(this)`. Ovaj izraz definiše značenje ključne riječi `this` unutar funkcije `handleClick`. Ovo je neophodno zbog načina na koji JavaScript tretira pokazivač `this`. Naime, u JavaScript-u se `this` podrazumijevano vezuje za kontekst u kome je pozvana funkcija, ne za objekat u kome je funkcija definisana. Drugim riječima, kada bi pozvali `handleClick` iz komponente `Dugme`, `this` bi unutar ove funkcije podrazumijevano pokazivao na element `button`, a ne na `Brojac`.

React-ov koncept jednosmjernog toka podataka može djelovati kao nepotrebna komplikacija, naročito programerima koji su navikli na *two-way data binding*. Međutim, ovaj pristup donosi značajne prednosti. Sa dvosmjernom komunikacijom se u nekim situacijama može javiti nepredviđeni tok podataka, jer i kontroler i prikaz mogu modifikovati stanje. Ovo značajno otežava održavanje velikih aplikacija. Međutim, ako podaci mogu da idu samo u jednom smjeru, tok podataka je uvijek predvidljiv. Prikaz na stranici zavisi samo od trenutnog *stanja* komponente. Jedini način da se promijeni stanje je izvršavanje određene *akcije*. Otkrivanje grešaka postaje mnogo jednostavnije zato što je uvijek jasno odakle potiču odgovarajući podaci. Takođe, aplikacija na ovaj način može raditi efikasnije, jer biblioteka uvijek zna granice svakog dijela sistema. 

[slika - jednosmjerni tok podataka - akcija > stanje > prikaz > ...]
https://flaviocopes.com/react-unidirectional-data-flow/
https://medium.com/@lizdenhup/understanding-unidirectional-data-flow-in-react-3e3524c09d8e
https://hashnode.com/post/why-does-react-emphasize-on-unidirectional-data-flow-and-flux-architecture-ciibz8ej600n2j3xtxgc0n1f0

### Liste i ključevi
Osnovna funkcija za rad sa kolekcijama u JavaScript-u je `map()`. Ova funkcija u argumentu prima callback funkciju i poziva je za svaki element kolekcije. Pri tome, `map` je čista funkcija, tj. ne modifikuje originalni niz već vraća novi kao rezultat. U JSX-u je prikaz elemenata liste veoma jednostavan pomoću ove funkcije:

```JavaScript
const brojevi = [1, 2, 3, 4, 5];
const listaBrojeva = numbers.map((broj) => 
    <li>{broj}</li>
);

ReactDOM.render(
    <ul>{listaBrojeva}</ul>,
    document.getElementById('root')
);
```

Prvo smo pomoću `map`  funkcije kreirali listu koja za svaki element niza `brojevi` sadrži `<li>` element sa odgovarajućim brojem. Zatim smo jednostavno tu listu dodali u `<ul>` element i prikazali ga u DOM-u. 

Kada prikazujemo listu na ovaj način, React će nas upozoriti da elementi liste ne sadrže ključ. Ključevi su jedinstveni identifikatori elemenata unutar jedne liste (ne moraju biti globalno jedinstveni). Oni pomažu React-u da elemente koji su dodati, uklonjeni ili promijenjeni. Podaci koje prikazujemo u listama najčešće potiču iz baze podataka pa već sadrže neki jedinstveni identifikator (primarni ključ) koji se može koristiti kao ključ. Korišćenje indeksa elemenata za ključeve se ne preporučuje jer u slučaju promjene redosljeda elemenata može negativno uticati na performanse. Ključ se može dodati na sljedeći način:

```JavaScript
const komentari = this.props.komentari; // niz objekata Komentar
const listaKomentara = komentari.map((komentar) =>
    <li key={komentar.id}>{komentar.tekst}</li>
);
```

Važno je napomenuti da ukoliko element liste izdvojimo u zasebnu komponentu, ključ se dodjeljuje komponenti, a ne `<li>` elementu unutar komponente:
```JavaScript
const listaKomentara = komentari.map((komentar) =>
    <Komentar key={komentar.id} komentar={komentar}/>
);
```

### Forme
HTML forme ne zahtijevaju dodatni JavaScript kod da bi funkcionisale. Klikom na dugme *submit* svi podaci iz polja za unos se šalju na odgovarajući URL pomoću HTTP poruke GET ili POST. Nakon slanja forme, browser podrazumijevano prelazi na neku novu stranicu koja je definisana u odgovoru servera. Ovo podrazumijevano ponašanje je zadovoljavajuće u većini slučajeva. Međutim, kontrolisanjem ponašanja forme pomoću JavaScript-a postiže se mnogo veća fleksibilnost. Ovo se u React-u postiže tehnikom *kontrolisanih* komponenti.

Polja za unos u React-u mogu biti *kontrolisana* i *nekontrolisana*. Nekontrolisana polja sama upravljaju svojim unutrašnjim stanjem i ažuriraju svoju vrijednost na osnovu unosa korisnika. Nedostatak ovog pristupa je što se ne može jednostavno pristupiti vrijednostima polja. U React-u se promjenljive vrijednosti obično čuvaju u *stanju* komponente. Povezivanjem React stanja sa vrijednošću polja za unos dobija se *kontrolisana* komponenta. Na ovaj način React stanje postaje "jedini izvor istine" za vrijednost polja i React ima potpunu kontrolu nad njegovom vrijednošću. Ovo se može implementirati na sljedeći način:

```JavaScript
class Forma extends React.Component {
    constructor(props) {
        super(props);
        this.state={
            ime: ""
        }
    }
    handleSubmit() {
        //Kod za slanje forme
    }
    handleChange(event) {
        this.setState({ime: event.target.value})
    }
    render() {
        <form onSubmit={this.handleSubmit}>
            <label for="ime">Unesi ime i prezime:</label>
            <input  type="text" 
                    value={this.state.ime} 
                    onChange={this.handleChange}
                    name="ime"/>
            <input type="submit" value="Pošalji"/>
        </form>
    }
}
```

Prvo smo postavili `value` atribut `input` polja na `this.state.ime`, čime smo obezbijedili da je vrijednost polja uvijek jednaka vrijednosti u stanju komponente. Kada korisnik promijeni vrijednost polja, poziva se funkcija `handleChange()` koja ažurira stanje komponente. 

Kontrolisane komponente su zahtjevnije za implementaciju ali omogućavaju značajna poboljšanja korisničkog iskustva. Jedan primjer je validacija korisničkog unosa u realnom vremenu. U formama za registraciju korisnika se često zahtijeva unos jedinstvenog korisničkog imena. Kod standardnih formi, ukoliko korisnik unese ime koje je već zauzeto, biće obaviješten o ovome tek nakon slanja forme. Kombinovanjem kontrolisanih komponenti i AJAX-a, aplikacija može u toku unosa imena ostvariti komunikaciju sa serverom i odmah obavijestiti korisnika o dostupnosti odabranog imena. 

### Podizanje stanja
Stanje čuva podatke koji pripadaju određenoj komponenti. Kako podaci teku u jednom smjeru, stanje komponente može da utiče samo na nju i na komponente koje se nalaze ispod nje u hijerarhiji. Prema tome, ako imamo više komoponenti koje rade sa istim promjenljivim podacima, poželjno je da se ti podaci čuvaju u stanju najviše zajedničke komponente koja radi sa tim podacima. Ovo se naziva "podizanje stanja". 

Na primjer, pretpostavimo da imamo komponentu `Proizvodi` u kojoj imamo dvije komponente - `PoljeZaPretragu` i `ListaProizvoda`. Želimo da implemetiramo filtriranje liste u realnom vremenu, tj. kada korisnik krene da kuca ime nekog proizvoda u polju za pretragu, lista se filtrira i prikazuju se samo proizvodi koji sadrže uneseni string. Prvi korak je implementirati polje za pretragu kao kontrolisanu komponentu, tj. vezati njenu vrijednost za stanje komponente. Međutim, ukoliko vrijednost polja čuvamo u stanju komponente `PoljeZaPretragu`, `ListaProizvda` neće imati pristup ovim podacima. Sada je potrebno izvršiti podizanje stanja, tj. vrijednost polja za pretragu je potrebno sačuvati u komponenti `Proizvodi`. Komponenta `Proizvodi` tada proslijeđuje funkciju `handleChange` polju za pretragu, koja će ažurirati stanje kada korisnik kuca na tastaturi, a listi proizvoda preko svojstava proslijeđuje trenutnu vrijednost polja za pretragu. 

```JavaScript
class Proizvodi extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            pretraga: ""
        };
        this.handleChange = this.handleChange.bind(this);
    }
    handleChange(event) {
        this.setState({
            pretraga: event.target.value
        });
    }
    render() {
        return(
            <PoljeZaPretragu 
                onChange={this.handleChange} 
                pretraga={this.state.pretraga}/>
            <ListaProizvoda 
                pretraga={this.state.pretraga}/>
        );
    }
}
class PoljeZaPretragu extends React.Component {
    render() {
        <input 
            type="text"
            value={this.props.pretraga} 
            onChange={this.props.onChange}/>
    }
}
class ListaProizvoda extends ReactComponent {
    // Komponenta može pristupiti vrijednosti polja za pretragu pomoću
    // this.props.pretraga i na osnovu nje ažurirati prikazanu listu. 
}
```

### Kompozicija umjesto nasljeđivanja
Nzm treba li mi ovo

## Postupak razvoja aplikacije
Sada ćemo ukratko proći kroz sve što smo do sada naučili o React-u i pokazati kako se ovi koncepti primjenjuju u razvoju web aplikacije. 

Razvoj web aplikacije, kao i svakog drugog projekta, počinje od planiranja. U slučaju korisničkog interfejsa, bilo bi poželjno nacrtati kako bi aplikacija trebala da izgleda. React aplikacija je izgrađena od komponenti pa je potrebno rastaviti korisnički interfejs na pojedinačne komponente. Pri tome, svaka komponenta bi trebala da bude odgovorna samo za jednu funkcionalnost. 

Kada imamo sliku željenog korisničkog interfejsa sa definisanom hijerarhijom komponenti, možemo započeti sa implementacijom. Najlakše je prvo kreirati statičku verziju, tj. definisati izgled svih potrebnih komponenti ali bez interaktivnosti. Proces definisanja izgleda obično zahtijeva dosta pisanja a malo razmišljanja, pa ga je dobro uraditi odvojeno od implementacije interaktivnosti.

U narednom koraku bitno je odrediti koje podatke je potrebno čuvati u stanju komponenti. Pri tome, stanje treba biti što kompaktnije, tj. ne treba čuvati suvišne podatke. Možemo se voditi tzv. "DRY" principom (Don't Repeat Yourself) tj. ne treba čuvati podatke koji se mogu jednostavno izračunati iz postojećih podataka. Na primjer, ako nam je potrebna lista nekih proizvoda i broj proizvoda na listi, potrebno je sačuvati samo listu jer broj njenih elemenata možemo dobiti pomoću JavaScript svojstva `length`. 

Nakon određivanja minimalnog seta promjenljivih podataka koji se čuvaju u stanju, treba odrediti gdje će se ovi podaci čuvati. Podsjetimo se da u React-u podaci teku u jednom smjeru, od vrha ka dnu. Prema tome, za svaki podatak je potrebno utvrditi kojim je komponentama potreban, a zatim odrediti prvu komponentu u hijerarhiji (od dna ka vrhu) koja je zajednička za ove komponente. U ovoj komponenti treba čuvati stanje, i implementirati potrebne funkcionalnosti vezane za ove podatke.  

Sada imamo definisanu hijerarhiju komponenti, njihovo ponašanje i tok podataka. U posljednjem koraku je potrebno implementirati funkcije koje omogućavaju "inverzni tok podataka", tj. definisati akcije kojima će komponente koje se nalaze niže u hijerarhiji modifikovati stanje odgovarajuće komponente. Treba napomenuti da ovo ne narušava React-ov jednosmjerni tok podataka jer su promjene podataka kontrolisane akcijama koje su definisane zajedno sa odgovarajućim stanjem. 

## React Router
S obzirom na to da se u single-page aplikacijama sav sadržaj nalazi na istoj HTML stranici, za promjenu prikaza na ekranu potrebno je mijenjati komponente koje se trenutno prikazuju, što nije uvijek jednostavno. Takođe, korisnici su navikli na određeno ponašanje web sajtova: klikom na link prelazi se na drugu stranicu, klikom na "back" vraćamo se na prethodnu, pri čemu se u browser-u uvijek prikazuje URL trenutne stranice. *React router* je paket koji nam omogućava jednostavnu implementaciju ovih funkcionalnosti i u single-page aplikaciji. Ovo podrazumijeva vezivanje određenih komponenti za odgovarajući URL i promjenu prikazane komponente pri kliku na link.

Ovaj paket se može instalirati pomoću npm-a:
```Shell
npm install react-router
```

Da bi se koristio unutar JavaScript fajla, potrebno ga je uključiti pomoću komande `import`. Pogledajmo sada jednostavan primjer rutiranja sa 3 komponente:

```JavaScript
import React from "react";
import { BrowserRouter as Router, Route, Link } from "react-router-dom";

function Index() {
  return <h2>Home</h2>;
}
function About() {
  return <h2>About</h2>;
}
function Users() {
  return <h2>Users</h2>;
}
function App() {
  return (
        <Router>
        <div>
            <nav>
                <ul>
                    <li>
                        <Link to="/">Home</Link>
                    </li>
                    <li>
                        <Link to="/about/">About</Link>
                    </li>
                    <li>
                        <Link to="/users/">Users</Link>
                    </li>
                </ul>
            </nav>
            <Route path="/" exact component={Index} />
            <Route path="/about/" component={About} />
            <Route path="/users/" component={Users} />
        </div>
        </Router>
  );
}

export default AppRouter;
```

Za kreiranje linkova u React router-u koristi se komponenta `Link` umjesto HTML tag-a `a`. Na mjestu gdje se treba prikazati odgovarajuća komponenta postavlja se element `Route` u kome se definiše komponenta koju prikazuje ta ruta, kao i URL za koji je vezana. 


## Prednosti u razvoju velikih aplikacija
U uvodnom dijelu je pomenuto da je React pogodan za kreiranje izuzetno kompleksnih aplikacija. Kroz ključne koncepte React-a smo sreli mnoge situacije u kojima se jednostavnost žrtvuje u korist povećanja preglednosti i smanjenja greški. Pogledajmo sada kako tačno ovaj trade-off doprinosi održivosti i proširivosti cjelokupne aplikacije.

Najznačajniji primjer je React-ov jednosmjerni tok podataka. Dvosmjerni tok (two way data binding) koji koriste mnogi framework-ovi je mnogo jednostavniji i razumljiviji za implementaciju. Komponenta može komunicirati sa komponentama iznad nje i ispod nje, sa potpunom slobodom, bez potrebe za proslijeđivanjem funkcija za modifikaciju. Međutim, pretpostavimo da imamo aplikaciju sa veoma velikim brojem komponenti, pri čemu mnoge komponente rade sa istim podacima. Ukoliko u ovoj situaciji saznamo da neki podatak ima pogrešnu vrijednost, veoma je teško pronaći izvor greške jer za nju može biti odgovorna bilo koja komponenta. React-ovo strogo pravilo nepromjenljivosti svojstava, jednosmjerni tok podataka i princip podizanja stanja postoje u cilju realizcije jedne ključne ideje - postojanja "samo jednog izvora istine" za svaki podatak. Drugim riječima, svaki promjenljivi podatak se nalazi samo na jednom mjestu u aplikaciji i može uticati samo na komponente ispod sebe u hijerarhiji. Jedini način da se ovaj podatak promijeni je izvršavanjem određenih akcija, koje su takođe definisane u komponenti u kojoj se nalazi stanje. Kada se neki dio aplikacije ponaša neočekivano, dovoljno je da se popnemo kroz hijerarhiju komponenti dok ne dođemo do komponente u kojoj je to stanje sačuvano. Kako je ovo jedini izvor tog podatka, veoma je lako pronaći i izvor greške.

Još jedan koncept koji na prvi pogled povećava količinu "suvišnog" koda je jednostavnost i fragmentacija komponenti. Međutim, činjenica da je jedna komponenta odgovorna samo za jednu funkcionalnost znatno olakšava održavanje aplikacija. Pri tome, na ovaj način se smanjuje ponavljanje koda jer se komponente mogu koristiti u različitim djelovima aplikacije i to u različitim kontekstima. Takođe, JSX omogućava da se i logika prikaza nalazi u komponenti što takođe olakšava održavanje i uklanja potrebu za ručnom manipulacijom DOM-a.



## Napredne tehnike
[The road to learn React]
[https://www.robinwieruch.de/reasons-why-i-moved-from-angular-to-react/]

Možda napravit kao rezime sa ukratko svim idejama reacta ili "Thinking in React" npr tu stavit jedan izvor istine 
When you see something wrong in the UI, you can use React Developer Tools to inspect the props and move up the tree until you find the component responsible for updating the state. This lets you trace the bugs to their source --- lessons learned iz lifting state up

Pomenuto je da su svojstva nepromjenljiva, podaci teku samo u jednom smjeru, stanje se podiže... Sve ovo je u cilju realizacije React-ove ideje o postojanju `Jedinstvenog izvora istine`.