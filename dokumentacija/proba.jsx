const name = "Andrej";
const element = <h1>Hello, {name}!</h1>

ReactDOM.render(
    element,
    document.getElementById('root')
);