vehicles
    id
    type string(car, truck, bike)
    year int  
    volume float
    power int
    price int
    km int
    transmission string
    fuel string
    consumption float
    featured bool  
    make_id fk
    model_id fk
    body_id fk
    user_id fk

users
    id
    name
    email string
    phone string
    password string

bodies
    id
    name string
    type string (car, truck, bike)

makes
    id
    name string
    type string (car, truck, bike)

models
    id
    name string
    type srting (car, truck, bike)
    make_id fk

photos
    id
    path string
    vehicle_id fk
