# Tutorial
- React je deklarativna, efikasna i fleksibilna JS biblioteka za pravljenje
  korisnickih interfejsa
- UI se sastavlja iz malih, izolovanih djelova koda koji se nazivaju
  komponentama - React.Component
- Komponenta uzima parametre *props* i vraca hijerarhiju viewova (pregleda)
  koji se prikazuju *render* metodom
- JSX - u toku kompajliranja se npr. <div /> sintaksa pretvara u
  React.createElement('div')
- JS se moze dodati bilo gdje u zagradama
- this.state - u konstruktoru komponente - pamti stanje te komponente
- sve react komponente koje imaju konstruktor trebaju da pocinju sa
  super(props)
- za prikupljanje podataka od vise djece, pozeljno je deklarisati njihovo
  stanje u roditeljskoj komponenti, a roditelji djeci prosljedjuju podatke
  pomocu *props*

# Documentation
- Osnovni *building blocks* su elementi i komponente

## JSX
- Primjer React elementa: 

```javascript
const element = <h1>Hello, world!</h1> 

ReactDOM.render(
    element,
    document.getElementById('root')
);
```

- Zasniva se na činjenici da je logika za renderovanje (prikaz?) usko povezana sa ostalom
  logikom korisničkog interfejsa (obrada događaja, promjena stanja, priprema
  podataka za prikaz)
- Umjesto razdvajanja *tehnologija* (odvajanje logike i izgleda), React
  razdvaja *interese* - osnovna jedinica je *komponenta* koja sadrži oboje
- Promjenljive možemo koristiti u JSX-u unutar HTML-a pomoću vitičastih
  zagrada:

```javascript
const name = 'Marko Marković';
const element = <h1>Hello, {name}</h1>;
```

- Osim promjenljivih, u vitičaste zagrade se može staviti bilo koji validan JS
  izraz
- Kad se koriste vitičaste zagrade za postavljanje HTML atributa, ne koriste se
  navodnici, npr. `<img src={url}></img>`
- Zaštita od *injection* napada - ReactDOM escape-uje vrijednosti iz JSX-a
  prije renderovanja - dozvoljeno je direktno dodavanje korisničkog unosa
- Prilikom kompajlovanja, JSX se pretvara u `React.createElement()` pozive.
- Ova funkcija kreira objekat - *React element* - koji predstavlja opis onoga
  što želimo vidjeti na ekranu

## Renderovanje Elemenata
- Elementi opisuju ono što želimo vidjeti na ekranu i predstavljaju najmanje
  gradivne element React aplikacija
- Elementi koji se već nalaze u HTML fajlu nazivaju se korijenim (*root*)
  DOM čvorovima
- React aplikacije obično imaju samo jedan korijeni DOM čvor

### Ažuriranje renderovanog elementa
- React elementi su *nepromjenljivi* - kada se jednom kreiraju ne mogu se
  promijeniti ni njihova djeca ni atributi - element predstavlja izgled
  korisničkog interfeja u određenom trenutku
- React samo ažurira ono što je potrebno!! - upoređuje novi element i njegovu djecu 
  sa prethodnim i mijenja samo djelove DOM-a koji će dovesti DOM u željeno stanje
- Ovo omogućava da razmišljamo o tome kako UI treba da izgleda u svakom
  trenutku, a ne šta treba promijeniti - ReactDOM će povesti brigu o tome - ovo
  eliminiše mnoge greške i olakšava rad

## Komponente i Props(svojstva)
- Komponente omogućavaju podjelu UI u nezavisne, ponovo iskoristive?reusable
  djelove, i posmatranje svake komponente izolovano
- Konceptualno, komponente su kao JS funkcije, primaju proizvoljne ulaze i
  vraćaju elemente koji opisuju šta se treba pokazati na ekranu
- mogu da se napišu kao JS funkcija ili kao ES6 klasa
- U JSX-u tagovi mogu da predstavljaju (osim DOM elemenata) i elemente koje je
  definisao korisnik, npr:

```javascript
function Welcome(props) {
    return <h1>Hello, {props.name}</h1>
}

const element - <Welcome name="Sara" />;
ReactDOM.render(
    element,
    document.getElementById('root')
);
```

- Komponente trebaju da budu što manje, to olakšava promjenu i reusability
- Svojstva je poželjno nazivati iz pogleda komponente, nezavisno od konteksta u
  kome se koristi (npr. komponenta Avatar koja se koristi za Komentar: nećemo
  nazvati `author` već `user`)

### Svojstva su nepromjenljiva
- Komponenta nikada ne smije da modifikuje svoja svojstva - "čiste funkcije"

## Stanje i životni ciklus
- Stanje - pripada komponenti i ona može da ga mijenja
- Početno stanje se definiše u konstruktoru
- Lifecycle methods: `componentDidMount()` i `componentWillUnmount()`
- Stanje se ne smije mijenjati direktno već pomoću `setState()`
- Stanja i svojstva se mogu ažurirati asinhrono pa se ne treba oslanjati na
  njihove vrijednosti za izračunavanje sljedećeg stanja
- Umjesto toga, koriste se arrow funkcije, koje će kao argumente primiti
  njihove vrijednosti u trenutku pozivanja
- Stanje nije dostupno ostalim komponentama, samo komponenti za koju je
  definisano
- Podaci mogu da putuju samo "naniže"

## Obrada događaja
- camelCase, proslijeđuje se fja a ne string: `<button onClick={activateLasers}`
- preventDefault se mora eksplicitno pozvati
- ne mora se podešavati listener DOM elementu nakon kreiranja, može se dodati
  kada se prvi put renderuje
### this
- generalno, ako referenciramo metod bez () poslije njega, moramo odraditi bind
  u konstruktoru
```javascript
constructor(props) {
    super(props);
    this.handleClick = this.handleClick.bind(this);
}
handleClick() {
    // Sada se this odnosi na komponentu, ne na dugme koje je pozvalo
}
```
- postoji i drugi način, da se koriste arrow ili eksperimentalne funkcije, ali
  ovo nije dobro u pogledu performansi jer se pri svakom kliku kreira nova
  instanca te funkcije i ako se podaci proslijeđuju dalje, može rezultovati
  nepotrebnim rerenderovanjima

## Uslovno renderovanje
- Mogu se kreirati različite komponente, a onda se renderuju samo neke od njih
  zavisno od stanja
- koriste se obične JS if...else 
- inline if - može se koristiti logički `&&` operator umjesto if
```javascript
{unreadMessages.length>0 &&
    <h2>Imas {unreadMessages.length} novih poruka</h2>
}
```
- ovo radi jer u JS `true && expression` uvijek daje `expression`
- ako ne želimo da se komponenta renderuje, vratiti null

## Liste i ključevi
- Primjer:
```javascript
const numbers = [1, 2, 3, 4, 5];
const listItems = numbers.map((number) =>
    <li>{number}</li>
);
ReactDOM.render(
    <ul>{listItems}</ul>
    document.getElementById('root')
);
```
- Svaki `li` bi trebao da ima kljuc
- https://medium.com/@robinpokorny/index-as-a-key-is-an-anti-pattern-e0349aece318
- Ako se neka komponenta izdvaja (npr. `ListItem` koji sadrži jedan `li`), ključ se dodjeljuje izdvojenoj komponenti (`ListItem`-u)

## HTML Forme
- HTML forme su drugačije jer podrazumijevano sadrže neko interno stanje
- Podrazumijevano ponašanje forme je prelazak na neku novu stranicu kad se
  pošalje
- Međutim, bolje je imati JS funkciju koja šalje formu
- Ovo se standardno postiže tehnikom "kontrolisanih komponenti"
- Element unosa forme čija je vrijednost kontrolisana od strane React-a naziva
  se kontrolisanom komponentom

## Podizanje stanja
- Trebao bi da postoji samo jedan *izvor istine* za svaki podatak koji se
  mijenja u React aplikaciji

## Kompozicija vs nasljeđivanje
- `children` prop - za prosljeđivanje djece direktno u izlaz komponente
- Ovo omogućava ostalim komponentama da im proslijede proizvoljne elemente
  ugniježđivanjem JSX-a

# Redux
- Čuva stanje aplikacije na jednom mjestu da bi svaka komponenta mogla da mu
  pristupi
- Bez njega, stanje mora biti u najvišoj zajedničkoj komponenti što otežava rad
  sa velikim hijerarhijama
- https://blog.logrocket.com/why-use-redux-reasons-with-clear-examples-d21bffd5835/

# Advanced Guides

## Fragmenti
- Kada komponenta renderuje više elemenata, mogu se grupisati da se ne bi
  dodavali nepotrebni elementi DOM-u

## Komponente višeg reda
- nije baš dio API-ja, već tehnika koja se koristi u React-u
- Konkretno, to je funkcija koja uzima komponentu i vraća drugu komponentu
