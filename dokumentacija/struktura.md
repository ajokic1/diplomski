# Uvod
- Danasnji razvoj i znacaj weba
- (povratak u proslost) Istorija WWW-a, web programiranja

## HTTP
- Sta je http
- Zahtjevi i odgovori, klijent i server

## HTML i CSS
- HTML - samo osnovna struktura i sadrzaj elemenata
- Stilizovanje HTML-a komplikovano -> CSS za izgled, raspored, polozaj
  elem(layout)

## JavaScript
- Uvod i interaktivnost
..- Java appleti, marketing - JavaScript
..- Standardizacija ECMAScript
..- ES6
- DOM
- AJAX

## JavaScript Frameworks
porast popularnosti SPA
Angular i ostale

# React
- SPA, frameworks, View u MVC
- Sta je React
- Istorija Reacta?
## JSX

## Elementi

## Komponente

## Svojstva

## Stanje

### Podizanje stanja

## Životni ciklus

## Obrada događaja
- this

## Uslovno renderovanje 

## Liste i ključevi

## Kompozicija umjesto nasljeđivanja

## Kontekst

## SPA i React Router

# Back end integracija

## Laravel

## REST API

# Proof of concept


