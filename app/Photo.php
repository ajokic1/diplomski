<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Photo extends Model
{
    protected $guarded = [];

    public function vehicle() {
        return $this->belongsTo('App\Vehicle');
    }
}
