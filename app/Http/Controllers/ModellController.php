<?php

namespace App\Http\Controllers;

use App\Modell;
use Illuminate\Http\Request;

class ModellController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return Modell::get(['id', 'name', 'type', 'make_id']);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {        
        
        return Modell::create($request->all());;
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Modell  $modell
     * @return \Illuminate\Http\Response
     */
    public function show(Modell $modell)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Modell  $modell
     * @return \Illuminate\Http\Response
     */
    public function edit(Modell $modell)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Modell  $modell
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Modell $modell)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Modell  $modell
     * @return \Illuminate\Http\Response
     */
    public function destroy(Modell $modell)
    {
        //
    }
}
