<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Vehicle extends Model
{
    protected $guarded = [];

    public function user() {
        return $this->belongsTo('App\User');
    }
    public function modell() {
        return $this->belongsTo('App\Modell');
    }
    public function make() {
        return $this->belongsTo('App\Make');
    }
    public function body() {
        return $this->belongsTo('App\Body');
    }
    public function photos() {
        return $this->hasMany('App\Photo');
    }
}
