<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Modell extends Model
{
    protected $guarded = [];

    public function vehicles() {
        return $this->hasMany('App\Vehicle');
    }
    public function make() {
        return $this->belongsTo('App\Make');
    }
}
