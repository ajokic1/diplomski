<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Body extends Model
{
    protected $guarded = [];

    public function vehicles() {
        return $this->hasMany('App\Vehicle');
    }
}
