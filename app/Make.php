<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Make extends Model
{
    protected $guarded = [];

    public function vehicles() {
        return $this->hasMany('App\Vehicle');
    }
    public function modells() {
        return $this->hasMany('App\Modell');
    }
}
