<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateVehiclesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('vehicles', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->timestamps();
            $table->enum('type', ['car','motorcycle','truck']);
            $table->unsignedInteger('year');
            $table->float('volume')->nullable();
            $table->unsignedInteger('power')->nullable();
            $table->unsignedInteger('price');
            $table->unsignedInteger('km')->nullable();
            $table->enum('transmission',['automatski', 'ručni']);
            $table->enum('fuel',['benzin', 'dizel', 'gas', 'električni', 'hibrid']);
            $table->float('consumption')->nullable();
            $table->boolean('featured')->default(false);
            $table->unsignedBigInteger('make_id');
            $table->unsignedBigInteger('modell_id');
            $table->unsignedBigInteger('body_id');
            $table->unsignedBigInteger('user_id');

            $table->foreign('make_id')->references('id')->on('makes');
            $table->foreign('modell_id')->references('id')->on('modells');
            $table->foreign('body_id')->references('id')->on('bodies');
            $table->foreign('user_id')->references('id')->on('users');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('vehicles');
    }
}
